package fr.soleil.bean.mca;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.MatteBorder;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.awt.ColorUtils;

public class ColorTestComparator {

    public static final int[] ZONE_COLORS = new int[] { ColorUtils.toRGBA(250, 165, 255, 255),
            ColorUtils.toRGBA(190, 220, 255, 255), ColorUtils.toRGBA(170, 255, 170, 255),
            ColorUtils.toRGBA(255, 220, 150, 255), ColorUtils.toRGBA(235, 235, 235, 255),
            ColorUtils.toRGBA(100, 255, 200, 255), ColorUtils.toRGBA(255, 200, 230, 255),
            ColorUtils.toRGBA(150, 220, 255, 255), ColorUtils.toRGBA(220, 255, 100, 255),
            ColorUtils.toRGBA(200, 200, 255, 255), ColorUtils.toRGBA(150, 250, 255, 255),
            ColorUtils.toRGBA(200, 255, 185, 255), ColorUtils.toRGBA(255, 230, 200, 255),
            ColorUtils.toRGBA(210, 210, 255, 255), ColorUtils.toRGBA(230, 255, 190, 255),
            ColorUtils.toRGBA(255, 190, 220, 255), ColorUtils.toRGBA(210, 240, 255, 255),
            ColorUtils.toRGBA(255, 230, 170, 255), ColorUtils.toRGBA(255, 255, 170, 255),
            ColorUtils.toRGBA(255, 220, 230, 255) };

    public ColorTestComparator() {
        super();
    }

    public static void main(String[] args) {

        try {
            final Chart chart = new Chart();
            chart.setManagementPanelVisible(false);
            chart.setFreezePanelVisible(false);
            chart.setDataViewCometeColor("data", CometeColor.RED);
            chart.setAutoScale(false, IChartViewer.Y1);
            chart.setAxisMinimum(0, IChartViewer.Y1);
            chart.setAxisMaximum(2, IChartViewer.Y1);
            chart.setAutoScale(false, IChartViewer.X);
            chart.setAxisMinimum(0, IChartViewer.X);
            chart.setAxisMaximum(100, IChartViewer.X);

            String[] labels = new String[ZONE_COLORS.length];
            double[] pos = new double[ZONE_COLORS.length];
            for (int i = 0; i < ZONE_COLORS.length; i++) {
                String id = "Roi" + (i + 1);
                CometeColor color = ColorTool.getCometeColor(new Color(ZONE_COLORS[i]));
                chart.setDataViewCometeColor(id, color);
                chart.setDataViewFillStyle(id, IChartViewer.FILL_STYLE_SOLID);
                chart.setDataViewMarkerCometeColor(id, color);
                chart.setDataViewCometeColor(id, color);
                chart.setDataViewLabelVisible(id, false);
                labels[i] = Integer.toString(i + 1);
                pos[i] = 5 * (i + 0.5);
            }
            // chart.getXAxis().setLabels(labels, pos);

            Thread dataThread = new Thread(ColorTestComparator.class.getName() + ": updating rois") {
                @Override
                public void run() {
                    Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
                    double max = Double.NEGATIVE_INFINITY;
                    double[] curveData = new double[200];
                    for (int i = 0; i <= 100; i++) {
                        double y = Math.random();
                        curveData[2 * i] = i;
                        curveData[2 * i + 1] = y;
                        if (y > max) {
                            max = y;
                        }
                    }
                    dataMap.put("Curve", curveData);
                    for (int i = 0; i < ColorTestComparator.ZONE_COLORS.length; i++) {
                        double[][] roiData = new double[2][2];
                        roiData[IChartViewer.X_INDEX] = new double[] { 5 * (i + 0.2), 5 * (i + 0.8) };
                        roiData[IChartViewer.Y_INDEX] = new double[] { max, max };
                        dataMap.put("Roi" + (i + 1), roiData);
                    }
                    chart.setData(dataMap);
                }
            };
            dataThread.start();

            JPanel color1Panel = new JPanel();
            color1Panel.setPreferredSize(new Dimension(50, 50));
            JPanel color2Panel = new JPanel();
            color2Panel.setPreferredSize(color1Panel.getPreferredSize());
            JPanel colorPanel = new JPanel();
            colorPanel.setLayout(new GridBagLayout());
            GridBagConstraints color1Constraints = new GridBagConstraints();
            color1Constraints.fill = GridBagConstraints.BOTH;
            color1Constraints.gridx = 0;
            color1Constraints.gridy = 0;
            color1Constraints.weightx = 0.5;
            color1Constraints.weighty = 1;
            GridBagConstraints color2Constraints = new GridBagConstraints();
            color2Constraints.fill = GridBagConstraints.BOTH;
            color2Constraints.gridx = 1;
            color2Constraints.gridy = 0;
            color2Constraints.weightx = 0.5;
            color2Constraints.weighty = 1;
            colorPanel.add(color1Panel, color1Constraints);
            colorPanel.add(color2Panel, color2Constraints);
            color1Panel.setBorder(new MatteBorder(0, 1, 0, 0, Color.BLACK));
            color2Panel.setBorder(new MatteBorder(0, 1, 0, 1, Color.BLACK));

            JPanel selectionPanel = new JPanel();
            JLabel selection1Label = new JLabel("Compare color", SwingConstants.RIGHT);
            ColorCombo selection1Combo = new ColorCombo(color1Panel);
            JLabel selection2Label = new JLabel("with color", SwingConstants.RIGHT);
            ColorCombo selection2Combo = new ColorCombo(color2Panel);
            selectionPanel.setLayout(new GridBagLayout());
            GridBagConstraints label1Constraints = new GridBagConstraints();
            label1Constraints.fill = GridBagConstraints.BOTH;
            label1Constraints.gridx = 0;
            label1Constraints.gridy = 0;
            label1Constraints.weightx = 0.5;
            label1Constraints.weighty = 1;
            GridBagConstraints combo1Constraints = new GridBagConstraints();
            combo1Constraints.fill = GridBagConstraints.NONE;
            combo1Constraints.gridx = 1;
            combo1Constraints.gridy = 0;
            combo1Constraints.weightx = 0;
            combo1Constraints.weighty = 0;
            combo1Constraints.insets = new Insets(0, 0, 0, 5);
            GridBagConstraints label2Constraints = new GridBagConstraints();
            label2Constraints.fill = GridBagConstraints.BOTH;
            label2Constraints.gridx = 2;
            label2Constraints.gridy = 0;
            label2Constraints.weightx = 0.5;
            label2Constraints.weighty = 1;
            GridBagConstraints combo2Constraints = new GridBagConstraints();
            combo2Constraints.fill = GridBagConstraints.NONE;
            combo2Constraints.gridx = 3;
            combo2Constraints.gridy = 0;
            combo2Constraints.weightx = 0;
            combo2Constraints.weighty = 0;
            combo2Constraints.insets = new Insets(0, 5, 0, 0);
            selectionPanel.add(selection1Label, label1Constraints);
            selectionPanel.add(selection1Combo, combo1Constraints);
            selectionPanel.add(selection2Label, label2Constraints);
            selectionPanel.add(selection2Combo, combo2Constraints);
            selectionPanel.setBackground(Color.WHITE);

            JPanel mainPanel = new JPanel();
            mainPanel.setLayout(new GridBagLayout());
            GridBagConstraints constraints1 = new GridBagConstraints();
            constraints1.fill = GridBagConstraints.BOTH;
            constraints1.gridx = 0;
            constraints1.gridy = 0;
            constraints1.weightx = 1;
            constraints1.weighty = 1;
            GridBagConstraints constraints2 = new GridBagConstraints();
            constraints2.fill = GridBagConstraints.HORIZONTAL;
            constraints2.gridx = 0;
            constraints2.gridy = 1;
            constraints2.weightx = 1;
            constraints2.weighty = 0;
            GridBagConstraints constraints3 = new GridBagConstraints();
            constraints3.fill = GridBagConstraints.HORIZONTAL;
            constraints3.gridx = 0;
            constraints3.gridy = 2;
            constraints3.weightx = 1;
            constraints3.weighty = 0;
            mainPanel.add(chart, constraints1);
            mainPanel.add(selectionPanel, constraints2);
            mainPanel.add(colorPanel, constraints3);

            JFrame frame = new JFrame("Color Test");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setContentPane(mainPanel);
            frame.setSize(800, 600);
            frame.setVisible(true);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}

class ColorCombo extends JComboBox<Object> {

    private static final long serialVersionUID = 59562802719595663L;

    private final JPanel panel;

    public ColorCombo(JPanel panel) {
        super();
        this.panel = panel;
        addActionListener(this);
        addItem("None");
        for (int i = 0; i < ColorTestComparator.ZONE_COLORS.length; i++) {
            addItem(new Integer(i + 1));
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object item = getSelectedItem();
        if (item instanceof Integer) {
            panel.setBackground(new Color(ColorTestComparator.ZONE_COLORS[((Integer) item).intValue() - 1]));
        } else {
            panel.setBackground(Color.WHITE);
        }
        panel.repaint();
    }

}
