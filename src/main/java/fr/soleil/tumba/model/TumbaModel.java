package fr.soleil.tumba.model;

import fr.soleil.lib.project.ObjectUtils;

public class TumbaModel {

    private String tumbaDeviceName;
    private String beamLineEnergyDeviceName;

    public TumbaModel() {
        super();
        tumbaDeviceName = ObjectUtils.EMPTY_STRING;
        beamLineEnergyDeviceName = ObjectUtils.EMPTY_STRING;
    }

    /**
     * @return Returns the tumbaDeviceName.
     */
    public String getTumbaDeviceName() {
        return tumbaDeviceName;
    }

    /**
     * @param diffractometer The diffractometer to set.
     */
    public void setTumbaDeviceName(String diffractometer) {
        if (diffractometer == null) {
            diffractometer = ObjectUtils.EMPTY_STRING;
        }
        if (!diffractometer.equals(tumbaDeviceName)) {
            tumbaDeviceName = diffractometer;
        }
    }

    /**
     * 
     * @return String beamLineEnergyDeviceName
     */
    public String getBeamLineEnergyDeviceName() {
        return beamLineEnergyDeviceName;
    }

    /**
     * 
     * @param beamLineEnergyDeviceName
     */
    public void setBeamLineEnergyDeviceName(String beamLineEnergyDeviceName) {
        this.beamLineEnergyDeviceName = beamLineEnergyDeviceName;
    }
}
