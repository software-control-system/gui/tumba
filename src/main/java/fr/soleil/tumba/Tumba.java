package fr.soleil.tumba;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.html.HTMLEditorKit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.application.Application;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.dialog.JDialogUtils;
import fr.soleil.tumba.bean.mca.MCABean;
import fr.soleil.tumba.model.TumbaModel;

public class Tumba extends Application {

    private static final long serialVersionUID = 6080254514637140609L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Tumba.class);
    private static final String ABOUT_FILENAME = "About.html";

    private static final ImageIcon SPLASH_BG = new ImageIcon(
            Tumba.class.getResource("/fr/soleil/tumba/icons/splash-11986-2.png"));
    private static final ImageIcon TUMBA_BG = new ImageIcon(
            Tumba.class.getResource("/fr/soleil/tumba/icons/tumba-image-freepng.fr.png"));
    private static final ImageIcon SMALL_ICON = new ImageIcon(
            Tumba.class.getResource("/fr/soleil/tumba/icons/tumba.png"));
    private static final String COPYRIGHT = "SOLEIL Synchrotron";

    private MCABean tumbaBean;
    protected TumbaModel model;

    public Tumba() {
        super();
    }

    /**
     * 
     * @return TumbaModel
     */
    public TumbaModel getModel() {
        return model;
    }

    /**
     * @param model The model to set.
     */
    public void setModel(TumbaModel model) {
        if (this.model == null) {
            if (model != null) {
                this.model = model;
                refreshGUI();
            }
        } else if (!this.model.equals(model)) {
            this.model = model;
            if (this.model == null) {
                clearGUI();
            } else {
                refreshGUI();
            }
        }
    }

    /**
     * This method initializes MCABean
     * 
     * @return MCABean
     */
    private MCABean getMCABean() {
        if (tumbaBean == null) {
            tumbaBean = new MCABean();
        }
        return tumbaBean;
    }

    private void refreshGUI() {
        // Bean
        tumbaBean.setModel(model.getTumbaDeviceName());
        tumbaBean.start();
        tumbaBean.setBeamLineEnergyDeviceName(model.getBeamLineEnergyDeviceName());
    }

    /**
     * 
     */
    private void clearGUI() {
        // MCABean
        tumbaBean.stop();
    }

    public void initializeTumba() {
        // TODO ResourceBundle
        ResourceBundle rb = ResourceBundle.getBundle("fr.soleil.tumba.application");
        String version = rb.getString("project.name") + " " + rb.getString("project.version") + " - "
                + rb.getString("build.date");
        setTitle(version);
        getSplash().setTitle(version);
        getSplash().setCopyright("Synchrotron SOLEIL");
        getSplash().setMaxProgress(3);
        getSplash().setAlwaysOnTop(true);
        getSplash().setMessage("Initializing components...");
        getSplash().progress(1);
        getSplash().setVisible(true);
        setContentPane(getMCABean());
        setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());

        getSplash().setMessage("Components initialized, connecting to MCA...");
        getSplash().progress(2);
        // getSplash().setVisible(false);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        final TumbaModel model = new TumbaModel();

        // Check devices
        if ((args != null) && (args.length > 0)) {
            model.setTumbaDeviceName(args[0]);
            if (args.length > 1) {
                model.setBeamLineEnergyDeviceName(args[1]);
            } else {
                System.out.println("\n-----\nbeamline device is not defined\n-----\n");
            }
        } else {
            System.out.println("Usage : [Counter] [Diffractometer]");
        }

        System.out.println("Initializing Tumba...");
        final Tumba application = new Tumba();
        application.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        application.initializeTumba();
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                application.setModel(model);
                return null;
            }

            @Override
            protected void done() {
                try {
                    get();
                    application.getSplash().setMessage("Components initialized, connecting to MCA...done");
                    System.out.println("Tumba initialization done");
                    application.getSplash().progress(3);
                    application.getSplash().setVisible(false);
                    System.out.println("Displaying Tumba application");
                    application.setVisible(true);
                    application.toFront();
                } catch (InterruptedException e) {
                    System.out.println("Tumba initialization interrupted!");
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    System.out.println("Tumba initialization error!");
                    e.getCause().printStackTrace();
                }
            }
        };
        worker.execute();
    }

    @Override
    protected Color getSplashForeground() {
        return Color.BLACK;
    }

    @Override
    protected ImageIcon getAboutDialogBackgroundImage() {
        return TUMBA_BG;
    }

    @Override
    protected ImageIcon getSplashImage() {
        return SPLASH_BG;
    }

    @Override
    protected Image getDefaultIconImage() {
        return SMALL_ICON.getImage();
    }

    @Override
    protected String getApplicationTitle() {
        return getClass().getSimpleName();
    }

    @Override
    protected String getCopyright() {
        return COPYRIGHT;
    }

    private static void openFileInJar(final String jarFileName, final String entryName, final String fileName,
            final String extension) {

        LOGGER.trace("jarFileName={}", jarFileName);
        LOGGER.trace("entryName={}", entryName);
        LOGGER.trace("fileName={}", fileName);
        LOGGER.trace("extension={}", extension);

        try (JarFile jarFile = new JarFile(jarFileName);) {
            JarEntry entry = jarFile.getJarEntry(entryName);
            InputStream is = jarFile.getInputStream(entry);

            // create a temp file named after the one from
            // the jar, with the same extension
            File tempFile = File.createTempFile(fileName, "." + extension);
            FileUtils.duplicateFile(is, tempFile, true);
            tempFile.deleteOnExit();
            LOGGER.trace("Extracting file to {}", tempFile.getAbsolutePath());

            Desktop.getDesktop().open(tempFile);

        } catch (IOException e) {
            LOGGER.error("Cannot open file {}", e.getMessage());
            LOGGER.trace("Stack trace", e);
        }
    }

    @Override
    protected JDialog generateAboutDialog() {
        JDialog aboutDialog = new JDialog(this, "About " + getApplicationTitle(), true);
        JEditorPane htmlPanel = new JEditorPane();
        htmlPanel.setEditorKit(new HTMLEditorKit());
        htmlPanel.setEditable(false);
        htmlPanel.setBackground(new Color(170, 170, 170));
        htmlPanel.addPropertyChangeListener("page", (e) -> {
            aboutDialog.pack();
            aboutDialog.setSize(aboutDialog.getWidth(), 780);
        });
        htmlPanel.addHyperlinkListener((e) -> {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                // if uri is a file, get a temp version extracted from the
                // jar and ask OS to open it,
                // else ask OS to browse the link
                if (Desktop.isDesktopSupported()) {
                    try {
                        URI uri = e.getURL().toURI();
                        String scheme = uri.getScheme();
                        String file = e.getURL().getFile();

                        if (scheme.equalsIgnoreCase("jar")) {
                            String[] pathInfo = file.split("!/");
                            String jarFileName = pathInfo[0].replaceFirst("file:", "");
                            String entryName = pathInfo[1];

                            String extension = FileUtils.getExtension(file);
                            String fileName = file.substring(file.lastIndexOf("/") + 1, file.lastIndexOf("."));
                            openFileInJar(jarFileName, entryName, fileName, extension);
                        } else if (scheme.equalsIgnoreCase("file")) {
                            Desktop.getDesktop().browse(uri);
                        }
                    } catch (Exception ex) {
                        LOGGER.error("Cannot open file {}", ex.getMessage());
                        LOGGER.trace("Stack trace", ex);
                    }
                }
            }
        });
        aboutDialog.setContentPane(htmlPanel);
        URL aboutURL = Tumba.class.getResource(ABOUT_FILENAME);
        try {
            htmlPanel.setPage(aboutURL);
            new Thread("Update version later") {
                @Override
                public void run() {
                    try {
                        sleep(200);
                        final StringBuilder versionBuilder = new StringBuilder();
                        ResourceBundle bundle = ResourceBundle
                                .getBundle(Tumba.class.getPackage().getName() + ".application");
                        versionBuilder.append(bundle.getString("project.version")).append(" (");
                        versionBuilder.append(bundle.getString("build.date")).append(")");
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                htmlPanel.setText(htmlPanel.getText().replace("X.X.X", versionBuilder.toString()));
                            }
                        });
                    } catch (InterruptedException e) {
                        // nothing to do;
                    }
                };
            }.start();
        } catch (IOException e) {
            LOGGER.error("Cannot open file {}", e.getMessage());
            LOGGER.trace("Stack trace", e);
        }
        // allow to close the dialog with Esc
        JDialogUtils.installEscapeCloseOperation(aboutDialog);

        return aboutDialog;
    }
}
