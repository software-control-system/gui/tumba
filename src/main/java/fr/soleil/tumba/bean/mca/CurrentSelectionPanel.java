package fr.soleil.tumba.bean.mca;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import fr.soleil.lib.project.ObjectUtils;

/**
 * 
 * @author ADIOUF
 * 
 */
public class CurrentSelectionPanel extends JPanel implements Observer {

    private static final long serialVersionUID = -609533160016422952L;

    private JPanel energySelectionPanel = null;
    private JPanel countSelectionPanel = null;
    private JPanel raySelectionPanel = null;
    private JPanel theoricalEnergyPanel = null;

    private JPanel elmentsPanel = null;

    private JLabel rayLabel = null;
    private JLabel rayValue = null;
    private JLabel theoricalEnergyLabel = null;
    private JLabel theoricalEnergyValue = null;
    private JLabel countLabel = null;
    private JLabel countValue = null;
    private JLabel energyLabel = null;
    private JLabel energyValue = null;
    private JTable energyTableValue = null;
    private TableModel tableModel = null;
    private JTextField energyList = null;

    /**
     * This is the default constructor
     */
    public CurrentSelectionPanel() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        Dimension d = new Dimension(10, 145);
        this.setPreferredSize(d);
        this.setLayout(new BorderLayout());
        this.add(getElmentsPanel(), BorderLayout.NORTH);
        this.add(new JScrollPane(getEnergyTableValue()), BorderLayout.CENTER);
        this.setBorder(BorderFactory.createTitledBorder("Current Selection"));
    }

    public JPanel getElmentsPanel() {
        if (elmentsPanel == null) {
            elmentsPanel = new JPanel();
            elmentsPanel.setLayout(new GridLayout(4, 1));
            elmentsPanel.add(getEnergySelectionPanel());
            elmentsPanel.add(getCountSelectionPanel());
            elmentsPanel.add(getRaySelectionPanel());
            elmentsPanel.add(getTheoricalEnergyPanel());
        }
        return elmentsPanel;
    }

    /**
     * 
     * @return
     */
    public JPanel getEnergySelectionPanel() {
        if (energySelectionPanel == null) {
            energySelectionPanel = new JPanel();
            energySelectionPanel.setLayout(new GridLayout(1, 2));
            energySelectionPanel.add(getEnergyLabel());
            energySelectionPanel.add(getEnergyValue());
        }
        return energySelectionPanel;
    }

    /**
     * method allowing to return the {@link JPanel} countSelectionPanel
     * 
     * @return {@link JPanel}
     */
    public JPanel getCountSelectionPanel() {
        if (countSelectionPanel == null) {
            countSelectionPanel = new JPanel();
            countSelectionPanel.setLayout(new GridLayout(1, 2));
            countSelectionPanel.add(getCountLabel());
            countSelectionPanel.add(getCountValue());
        }
        return countSelectionPanel;
    }

    /**
     * method allowing to return the {@link JPanel} raySelectionPanel
     * 
     * @return {@link JPanel}
     */
    public JPanel getRaySelectionPanel() {
        if (raySelectionPanel == null) {
            raySelectionPanel = new JPanel();
            raySelectionPanel.setLayout(new GridLayout(1, 2));
            raySelectionPanel.add(getRayLabel());
            raySelectionPanel.add(getRayValue());
        }
        return raySelectionPanel;
    }

    public JPanel getTheoricalEnergyPanel() {
        if (theoricalEnergyPanel == null) {
            theoricalEnergyPanel = new JPanel();
            theoricalEnergyPanel.setLayout(new GridLayout(1, 2));
            theoricalEnergyPanel.add(getTheoricalEnergyLabel());
            theoricalEnergyPanel.add(getTheoricalEnergyValue());
        }
        return theoricalEnergyPanel;
    }

    /**
     * method allowing to return the {@link JLabel} energyLabel1
     * 
     * @return {@link JLabel}
     */
    public JLabel getEnergyLabel() {
        if (energyLabel == null) {
            energyLabel = new JLabel("Energy (eV) : ");
            energyLabel.setFont(new Font("Default", Font.BOLD, 15));
        }
        return energyLabel;
    }

    /**
     * 
     * @return
     */
    public JLabel getEnergyValue() {
        if (energyValue == null) {
            energyValue = new JLabel("0");
            energyValue.setFont(new Font("Default", Font.BOLD, 15));
        }
        return energyValue;
    }

    /**
     * 
     * @return
     */
    public JTextField getEnergyList() {
        if (energyList == null) {
            energyList = new JTextField();
            energyList.setFont(new Font("Default", Font.BOLD, 15));
            energyList.setEditable(false);
        }
        return energyList;
    }

    public JTable getEnergyTableValue() {
        if (energyTableValue == null) {

            String[] columnNames = { "Ka1", "Ka2", "Kb1", "La1", "La2", "Lb1", "Lb2", "Lg1" };
            Object[][] cellules = { { ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING,
                    ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING,
                    ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING } };

            tableModel = new DefaultTableModel(cellules, columnNames);
            energyTableValue = new JTable(tableModel);
            energyTableValue.setEnabled(false);
        }
        return energyTableValue;
    }

    /**
     * method allowing to return the {@link JLabel} countLabel1
     * 
     * @return {@link JLabel}
     */
    public JLabel getCountLabel() {
        if (countLabel == null) {
            countLabel = new JLabel("Count : ");
            countLabel.setFont(new Font("Default", Font.BOLD, 15));
        }
        return countLabel;
    }

    /**
     * method allowing to return the {@link JLabel} countLabel2
     * 
     * @return {@link JLabel}
     */
    public JLabel getCountValue() {
        if (countValue == null) {
            countValue = new JLabel("0");
            countValue.setFont(new Font("Default", Font.BOLD, 15));
        }
        return countValue;
    }

    /**
     * method allowing to return the {@link JLabel} rayLabel1
     * 
     * @return {@link JLabel}
     */
    public JLabel getRayLabel() {
        if (rayLabel == null) {
            rayLabel = new JLabel("Ray : ");
            rayLabel.setFont(new Font("Default", Font.BOLD, 15));
        }
        return rayLabel;
    }

    /**
     * method allowing to return the {@link JLabel} rayLabel2
     * 
     * @return {@link JLabel}
     */
    public JLabel getRayValue() {
        if (rayValue == null) {
            rayValue = new JLabel("0");
            rayValue.setFont(new Font("Default", Font.BOLD, 15));
        }
        return rayValue;
    }

    /**
     * method allowing to return the {@link JLabel} rayLabel3
     * 
     * @return {@link JLabel}
     */
    public JLabel getTheoricalEnergyLabel() {
        if (theoricalEnergyLabel == null) {
            theoricalEnergyLabel = new JLabel("Theorical energy (eV) : ");
            theoricalEnergyLabel.setFont(new Font("Default", Font.BOLD, 15));
        }
        return theoricalEnergyLabel;
    }

    /**
     * method allowing to return the {@link JLabel} rayLabel4
     * 
     * @return {@link JLabel}
     */
    public JLabel getTheoricalEnergyValue() {
        if (theoricalEnergyValue == null) {
            theoricalEnergyValue = new JLabel("0");
            theoricalEnergyValue.setFont(new Font("Default", Font.BOLD, 15));
        }
        return theoricalEnergyValue;
    }

    /**
     * 
     * @param configuration
     */
    @Override
    public void update(Observable o, Object arg) {

    }

    /**
     * 
     */
    protected void refreshGUI(String[] currentSelection, List<Float> elementList, MCABean bean) {
        energyValue.setText(currentSelection[0]);
        countValue.setText(currentSelection[1]);
        rayValue.setText(currentSelection[2]);
        theoricalEnergyValue.setText(currentSelection[3]);

        for (int i = 2; i < elementList.size(); i++) {
            tableModel.setValueAt(elementList.get(i), 0, i - 2);
        }
    }
}
