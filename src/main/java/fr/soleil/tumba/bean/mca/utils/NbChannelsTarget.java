package fr.soleil.tumba.bean.mca.utils;

import java.lang.ref.WeakReference;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.lib.project.ObjectUtils;

public class NbChannelsTarget implements INumberTarget {

    private final WeakReference<IChannelCountManager> channelCountManagerReference;

    public NbChannelsTarget(IChannelCountManager channelCountManager) {
        super();
        channelCountManagerReference = channelCountManager == null ? null : new WeakReference<>(channelCountManager);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public Number getNumberValue() {
        IChannelCountManager manager = ObjectUtils.recoverObject(channelCountManagerReference);
        return Integer.valueOf(manager == null ? 0 : manager.getChannelCount());
    }

    @Override
    public void setNumberValue(Number value) {
        int newValue = (value == null ? 0 : value.intValue());
        if (newValue < 0) {
            newValue = 0;
        }
        IChannelCountManager manager = ObjectUtils.recoverObject(channelCountManagerReference);
        if (manager != null) {
            manager.setChannelCount(newValue);
        }
    }

}
