package fr.soleil.tumba.bean.mca.utils;

import java.lang.ref.WeakReference;
import java.util.EventObject;

import fr.soleil.comete.definition.listener.ISpinnerListener;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.MathConst;

public class ChannelListener implements INumberTarget, ISpinnerListener {

    private final WeakReference<IChannelManager> channelManagerRef;

    public ChannelListener(IChannelManager channelManager) {
        super();
        this.channelManagerRef = channelManager == null ? null : new WeakReference<>(channelManager);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public Number getNumberValue() {
        // not managed
        return null;
    }

    @Override
    public void setNumberValue(Number value) {
        IChannelManager channelManager = ObjectUtils.recoverObject(channelManagerRef);
        if (channelManager != null) {
            channelManager.channelChanged(value == null ? MathConst.NAN_FOR_NULL : value.doubleValue());
        }
    }

    @Override
    public void valueChanged(EventObject event) {
        updateChannel();
    }

    public void updateChannel() {
        IChannelManager channelManager = ObjectUtils.recoverObject(channelManagerRef);
        INumberTarget channelSetter = channelManager == null ? null : channelManager.getChannelSetter();
        if (channelSetter != null) {
            setNumberValue(channelSetter.getNumberValue());
        }
    }
}