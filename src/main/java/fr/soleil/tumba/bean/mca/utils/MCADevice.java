package fr.soleil.tumba.bean.mca.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public enum MCADevice implements MCAConstants {

    XIA(XIA_DXP, true), DANTE(DANTE_DPP, true), XSPRESS(XSPRESS3, false), UNKNOWN(UNKNOWN_DEVICE, false);

    // attribute titles
    private static final String TITLE_LIVE_TIME = "Live Time";
    private static final String TITLE_REAL_TIME = "Real Time";
    private static final String TITLE_TRIGGER_LIVE_TIME = "Trigger Live Time";
    private static final String TITLE_DEAD_TIME = "Dead Time";
    private static final String TITLE_EVENTS_IN_RUN = "Events in Run";
    private static final String TITLE_INPUT_COUNT_RATE = "Input Count Rate";
    private static final String TITLE_OUTPUT_COUNT_RATE = "Output Count Rate";
    private static final String TITLE_NB_MODULES = "Nb Modules";
    private static final String TITLE_NB_CHANNELS = "Nb Channels";
    private static final String TITLE_NB_BINS = "Nb Bins";
    private static final String TITLE_CURRENT_ALIAS = "Current Alias";
    private static final String TITLE_CURRENT_MODE = "Current Mode";
    private static final String TITLE_CURRENT_CONFIG_FILE = "Current Config File";
    private static final String TITLE_PRESET_VALUE = "Preset Value";
    private static final String TITLE_NB_PIXELS = "Nb Pixels";
    private static final String TITLE_PRESET_TYPE = "Preset Type";
    private static final String TITLE_CURRENT_PIXEL = "Current Pixel";
    private static final String TITLE_BOARD_TYPE = "Board Type";
    private static final String TITLE_TRIGGER_MODE = "Trigger Mode";
    private static final String TITLE_SP_TIME = "Spectrum Time";
    private static final String TITLE_NB_FRAMES = "Nb Frames";
    private static final String TITLE_CURRENT_FRAME = "Current Frame";
    private static final String TITLE_EXPOSURE_TIME = "Exposure Time";

    // attribute values
    private static final String NONE = "NONE";
    private static final String FIXED_REAL = "FIXED_REAL";
    private static final String FIXED_LIVE = "FIXED_LIVE";
    private static final String FIXED_EVENTS = "FIXED_EVENTS";
    private static final String FIXED_TRIGGERS = "FIXED_TRIGGERS";
    private static final String INTERNAL_MULTI = "INTERNAL_MULTI";
    private static final String EXTERNAL_GATE = "EXTERNAL_GATE";
    private static final String INTERNAL = "INTERNAL";
    private static final String GATE = "GATE";

    // attributes configuration
    private static final Collection<DeviceMode> MCA_ONLY = Arrays.asList(DeviceMode.MCA);
    private static final Collection<DeviceMode> MAPPING_ONLY = Arrays.asList(DeviceMode.MAPPING);
//    private static final Collection<DeviceMode> SCA_ONLY = Arrays.asList(DeviceMode.MCA);
    private static final Collection<DeviceMode> MCA_MAPPING = Arrays.asList(DeviceMode.MCA, DeviceMode.MAPPING);
    private static final Collection<DeviceMode> MAPPING_SCA = Arrays.asList(DeviceMode.MAPPING, DeviceMode.SCA);
//    private static final Collection<DeviceMode> MCA_SCA = Arrays.asList(DeviceMode.MCA, DeviceMode.SCA);
    private static final Collection<DeviceMode> MCA_MAPPING_SCA = Arrays.asList(DeviceMode.MCA, DeviceMode.MAPPING,
            DeviceMode.SCA);
    private static final Collection<DeviceMode> ALL_MODES = Arrays.asList(DeviceMode.values());

    static {
        ////////////////////////////////
        // Register XIA specificities //
        ////////////////////////////////

        // XIA ROIs
        XIA.registerRoiModes(DeviceMode.MCA);
        XIA.registerSpecialRoiModes(DeviceMode.SCA);
        // XIA info attributes
        XIA.addInfoAttribute(LIVE_TIME, TITLE_LIVE_TIME);
        XIA.addInfoAttribute(REAL_TIME, TITLE_REAL_TIME);
        XIA.addInfoAttribute(TRIGGER_LIVE_TIME, TITLE_TRIGGER_LIVE_TIME);
        XIA.addInfoAttribute(DEAD_TIME, TITLE_DEAD_TIME);
        XIA.addInfoAttribute(EVENTS_IN_RUN, TITLE_EVENTS_IN_RUN);
        XIA.addInfoAttribute(INPUT_COUNT_RATE, TITLE_INPUT_COUNT_RATE);
        XIA.addInfoAttribute(OUTPUT_COUNT_RATE, TITLE_OUTPUT_COUNT_RATE);
        // XIA control attributes
        XIA.addControlAttribute(NB_MODULES, TITLE_NB_MODULES, MCA_MAPPING_SCA, null);
        XIA.addControlAttribute(NB_CHANNELS, TITLE_NB_CHANNELS, MCA_MAPPING_SCA, null);
        XIA.addControlAttribute(NB_BINS, TITLE_NB_BINS, MCA_MAPPING_SCA, null);
        XIA.addControlAttribute(CURRENT_ALIAS, TITLE_CURRENT_ALIAS, MCA_MAPPING_SCA, null);
        XIA.addControlAttribute(CURRENT_MODE, TITLE_CURRENT_MODE, ALL_MODES, null);
        XIA.addControlAttribute(CURRENT_CONFIG_FILE, TITLE_CURRENT_CONFIG_FILE, ALL_MODES, null);
        XIA.addControlAttribute(PRESET_VALUE, TITLE_PRESET_VALUE, MCA_ONLY, MCA_ONLY);
        XIA.addControlAttribute(NB_PIXELS, TITLE_NB_PIXELS, MAPPING_SCA, MAPPING_SCA);
        XIA.registerStepAttribute(NB_PIXELS, 1, Double.POSITIVE_INFINITY, 1, 1);
        XIA.addControlAttribute(PRESET_TYPE, TITLE_PRESET_TYPE, MCA_ONLY, MCA_ONLY);
        XIA.registerLimitedValues(PRESET_TYPE, NONE, FIXED_REAL, FIXED_LIVE, FIXED_EVENTS, FIXED_TRIGGERS);
        XIA.addControlAttribute(CURRENT_PIXEL, TITLE_CURRENT_PIXEL, MAPPING_SCA, null);
        XIA.addControlAttribute(BOARD_TYPE, TITLE_BOARD_TYPE, ALL_MODES, null);

        //////////////////////////////////
        // Register DANTE specificities //
        //////////////////////////////////

        // DANTE ROIs
        DANTE.registerRoiModes(DeviceMode.MCA, DeviceMode.MAPPING);
        // DANTE info attributes
        DANTE.addInfoAttribute(LIVE_TIME, TITLE_LIVE_TIME);
        DANTE.addInfoAttribute(REAL_TIME, TITLE_REAL_TIME);
        DANTE.addInfoAttribute(DEAD_TIME, TITLE_DEAD_TIME);
        DANTE.addInfoAttribute(EVENTS_IN_RUN, TITLE_EVENTS_IN_RUN);
        DANTE.addInfoAttribute(INPUT_COUNT_RATE, TITLE_INPUT_COUNT_RATE);
        DANTE.addInfoAttribute(OUTPUT_COUNT_RATE, TITLE_OUTPUT_COUNT_RATE);
        // DANTE control attributes
        DANTE.addControlAttribute(NB_MODULES, TITLE_NB_MODULES, MCA_MAPPING, null);
        DANTE.addControlAttribute(NB_CHANNELS, TITLE_NB_CHANNELS, MCA_MAPPING, null);
        DANTE.addControlAttribute(NB_BINS, TITLE_NB_BINS, MCA_MAPPING, null);
        DANTE.addControlAttribute(CURRENT_ALIAS, TITLE_CURRENT_ALIAS, MCA_MAPPING, null);
        DANTE.addControlAttribute(CURRENT_MODE, TITLE_CURRENT_MODE, ALL_MODES, null);
        DANTE.addControlAttribute(CURRENT_CONFIG_FILE, TITLE_CURRENT_CONFIG_FILE, ALL_MODES, null);
        DANTE.addControlAttribute(TRIGGER_MODE, TITLE_TRIGGER_MODE, MCA_MAPPING, MAPPING_ONLY);
        DANTE.registerLimitedValues(TRIGGER_MODE, INTERNAL_MULTI, EXTERNAL_GATE);
        DANTE.addControlAttribute(PRESET_VALUE, TITLE_PRESET_VALUE, MCA_ONLY, MCA_ONLY);
        DANTE.addControlAttribute(NB_PIXELS, TITLE_NB_PIXELS, MAPPING_ONLY, MAPPING_ONLY);
        DANTE.registerStepAttribute(NB_PIXELS, 1, Double.POSITIVE_INFINITY, 1, 1);
        DANTE.addControlAttribute(PRESET_TYPE, TITLE_PRESET_TYPE, MCA_ONLY, null);
        DANTE.addControlAttribute(CURRENT_PIXEL, TITLE_CURRENT_PIXEL, MAPPING_ONLY, null);
        DANTE.addControlAttribute(BOARD_TYPE, TITLE_BOARD_TYPE, ALL_MODES, null);
        DANTE.addControlAttribute(SP_TIME, TITLE_SP_TIME, MAPPING_ONLY, MAPPING_ONLY);
        DANTE.registerStepAttribute(SP_TIME, 0, Double.POSITIVE_INFINITY, 1, 1);

        ////////////////////////////////////
        // Register XSPRESS specificities //
        ////////////////////////////////////

        // XSPRESS ROIs
        XSPRESS.registerRoiModes(DeviceMode.MAPPING);
        // XSPRESS info attributes
        XSPRESS.addInfoAttribute(REAL_TIME, TITLE_REAL_TIME);
        XSPRESS.addInfoAttribute(DEAD_TIME, TITLE_DEAD_TIME);
        XSPRESS.addInfoAttribute(EVENTS_IN_RUN, TITLE_EVENTS_IN_RUN);
        XSPRESS.addInfoAttribute(INPUT_COUNT_RATE, TITLE_INPUT_COUNT_RATE);
        XSPRESS.addInfoAttribute(OUTPUT_COUNT_RATE, TITLE_OUTPUT_COUNT_RATE);
        // XSPRESS control attributes
        XSPRESS.addControlAttribute(NB_MODULES, TITLE_NB_MODULES, MAPPING_ONLY, null);
        XSPRESS.addControlAttribute(NB_CHANNELS, TITLE_NB_CHANNELS, MAPPING_ONLY, null);
        XSPRESS.addControlAttribute(NB_BINS, TITLE_NB_BINS, MAPPING_ONLY, null);
        XSPRESS.addControlAttribute(NB_FRAMES, TITLE_NB_FRAMES, MAPPING_ONLY, MAPPING_ONLY);
        XSPRESS.addControlAttribute(CURRENT_FRAME, TITLE_CURRENT_FRAME, MAPPING_ONLY, null);
        XSPRESS.addControlAttribute(CURRENT_MODE, TITLE_CURRENT_MODE, ALL_MODES, null);
        XSPRESS.addControlAttribute(BOARD_TYPE, TITLE_BOARD_TYPE, MAPPING_ONLY, null);
        XSPRESS.addControlAttribute(EXPOSURE_TIME, TITLE_EXPOSURE_TIME, MAPPING_ONLY, MAPPING_ONLY);
        XSPRESS.addControlAttribute(TRIGGER_MODE, TITLE_TRIGGER_MODE, MAPPING_ONLY, MAPPING_ONLY);
        XSPRESS.registerLimitedValues(TRIGGER_MODE, INTERNAL, GATE);
    }

    private final String deviceClass;
    private final List<String> infoAttributes, infoAttributesFinal;
    private final List<String> controlAttributes, controlAttributesFinal;
    private final List<String> stateIndependantControlAttributes, stateIndependantControlAttributesFinal;
    private final Collection<DeviceMode> roiModes, specialRoiModes;
    private final Map<String, String> attributeTitles;
    private final Map<String, Collection<DeviceMode>> availabilityMap, editabilityMap;
    private final Map<String, Number[]> stepAttributes; // min, max, step, initial value
    private final Map<String, String[]> limitedValuesAttributes;
    private final boolean loadConfigFileEnabled;

    private MCADevice(String deviceClass, boolean loadConfigFileEnabled) {
        this.deviceClass = deviceClass;
        this.loadConfigFileEnabled = loadConfigFileEnabled;
        infoAttributes = new ArrayList<>();
        infoAttributesFinal = Collections.unmodifiableList(infoAttributes);
        controlAttributes = new ArrayList<>();
        controlAttributesFinal = Collections.unmodifiableList(controlAttributes);
        stateIndependantControlAttributes = new ArrayList<>();
        stateIndependantControlAttributesFinal = Collections.unmodifiableList(stateIndependantControlAttributes);
        attributeTitles = new HashMap<>();
        availabilityMap = new HashMap<>();
        editabilityMap = new HashMap<>();
        stepAttributes = new HashMap<>();
        limitedValuesAttributes = new HashMap<>();
        roiModes = new HashSet<>();
        specialRoiModes = new HashSet<>();
    }

    /**
     * Returns whether "load config file" is enabled with this {@link MCADevice}.
     * 
     * @return Whether "load config file" is enabled with this {@link MCADevice}.
     */
    public boolean isLoadConfigFileEnabled() {
        return loadConfigFileEnabled;
    }

    /**
     * Registers an info attribute in this {@link MCADevice}.
     * 
     * @param attribute The attribute name.
     * @param title The titled that should be displayed for this attribute.
     */
    private void addInfoAttribute(String attribute, String title) {
        infoAttributes.add(attribute);
        attributeTitles.put(attribute, title);
    }

    /**
     * Registers a control attribute in this {@link MCADevice}.
     * 
     * @param attribute The attribute name.
     * @param title The titled that should be displayed for this attribute.
     * @param availability The modes in which this attribute is available.
     * @param editability The modes in which this attribute is writable.
     */
    private void addControlAttribute(String attribute, String title, Collection<DeviceMode> availability,
            Collection<DeviceMode> editability) {
        controlAttributes.add(attribute);
        stateIndependantControlAttributes.add(attribute);
        attributeTitles.put(attribute, title);
        if (availability != null) {
            availabilityMap.put(attribute, availability);
        }
        if (editability != null) {
            editabilityMap.put(attribute, editability);
        }
    }

    /**
     * Registers the min, max, step and initial value of an attribute.
     * 
     * @param attribute The attribute name.
     * @param min The attribute minimum value.
     * @param max The attribute maximum value.
     * @param step The preferred step to set attribute with (when editing with a spinner).
     * @param initialValue The attribute initial value (initial value for a spinner).
     */
    private void registerStepAttribute(String attribute, Number min, Number max, Number step, Number initialValue) {
        stepAttributes.put(attribute, new Number[] { min, max, step, initialValue });
    }

    /**
     * Registers the only possible values of an attribute.
     * 
     * @param attribute The attribute name.
     * @param values The only possible values.
     */
    private void registerLimitedValues(String attribute, String... values) {
        limitedValuesAttributes.put(attribute, values);
    }

    /**
     * Registers the modes at which ROIs are available.
     * 
     * @param modes The modes at which ROIs are available.
     */
    private void registerRoiModes(DeviceMode... modes) {
        if (modes != null) {
            for (DeviceMode mode : modes) {
                roiModes.add(mode);
            }
        }
    }

    /**
     * Registers the modes at which special ROIs management should be used.
     * 
     * @param modes The modes at which special ROIs management should be used.
     */
    private void registerSpecialRoiModes(DeviceMode... modes) {
        if (modes != null) {
            for (DeviceMode mode : modes) {
                specialRoiModes.add(mode);
                roiModes.add(mode);
            }
        }
    }

    /**
     * Calculates whether a {@link DeviceMode} is registered for an attribute in given {@link Map}.
     * 
     * @param attribute The attribute.
     * @param mode The {@link DeviceMode}.
     * @param map The {@link Map}.
     * @return A <code>boolean</code>.
     */
    private boolean isModeCompatible(String attribute, DeviceMode mode, Map<String, Collection<DeviceMode>> map) {
        Collection<DeviceMode> modes = map.get(attribute);
        return (modes != null) && modes.contains(mode);
    }

    /**
     * Returns the title that should be displayed for given attribute.
     * 
     * @param attribute The attribute.
     * @return A {@link String}.
     */
    public String getAttributeTitle(String attribute) {
        String title;
        if (attribute == null) {
            title = attribute;
        } else {
            title = attributeTitles.get(attribute);
            if (title == null) {
                title = attribute;
            }
        }
        return title;
    }

    /**
     * Returns the info attributes.
     * 
     * @return A {@link List}.
     */
    public List<String> getInfoAttributes() {
        return infoAttributesFinal;
    }

    /**
     * Returns the control attributes.
     * 
     * @return A {@link List}.
     */
    public List<String> getControlAttributes() {
        return controlAttributesFinal;
    }

    /**
     * Returns the control attributes the writability of which don't depend on device's state.
     * 
     * @return A {@link List}.
     */
    public List<String> getStateIndependantControlAttributes() {
        return stateIndependantControlAttributesFinal;
    }

    /**
     * Returns the known min, max, step and initial value for given attribute.
     * 
     * @param attribute The attribute.
     * @return A {@link Number} array: <code>{min, max, step, initialValue}</code>. May be <code>null</code>.
     */
    public Number[] getStepConfiguration(String attribute) {
        return stepAttributes.get(attribute);
    }

    /**
     * Returns the known only possible values for given attribute.
     * 
     * @param attribute The attribute.
     * @return A {@link String} array. Can be <code>null</code>.
     */
    public String[] getPossibleValues(String attribute) {
        return limitedValuesAttributes.get(attribute);
    }

    /**
     * Returns whether given attribute may be writable (depending on {@link DeviceMode} or device state).
     * 
     * @param attribute The attribute.
     * @return A <code>boolean</code>.
     */
    public boolean mayBeWritable(String attribute) {
        return (attribute != null) && editabilityMap.containsKey(attribute);
    }

    /**
     * Returns whether given attribute is available in given {@link DeviceMode}.
     * 
     * @param attribute The attribute.
     * @param mode The {@link DeviceMode}.
     * @return A <code>boolean</code>.
     */
    public boolean isAvailableInMode(String attribute, DeviceMode mode) {
        return isModeCompatible(attribute, mode, availabilityMap);
    }

    /**
     * Returns whether given attribute is writable in given {@link DeviceMode}.
     * 
     * @param attribute The attribute.
     * @param mode The {@link DeviceMode}.
     * @return A <code>boolean</code>.
     */
    public boolean isWritableInMode(String attribute, DeviceMode mode) {
        return isModeCompatible(attribute, mode, editabilityMap);
    }

    /**
     * Returns whether ROIs management is available in given {@link DeviceMode}.
     * 
     * @param mode The {@link DeviceMode}.
     * @return A <code>boolean</code>.
     */
    public boolean areRoisEnabledInMode(DeviceMode mode) {
        return roiModes.contains(mode);
    }

    /**
     * Returns whether special ROIs management should be used with given {@link DeviceMode}.
     * 
     * @param mode The {@link DeviceMode}.
     * @return A <code>boolean</code>.
     */
    public boolean isSpecialRoiMode(DeviceMode mode) {
        return specialRoiModes.contains(mode);
    }

    /**
     * Parses a {@link String} to recover best matching {@link MCADevice}.
     * 
     * @param deviceClass The {@link String} to parse. It is expected to be a device class.
     * @return A {@link MCADevice}. Never <code>null</code>.
     */
    public static MCADevice parseMCADevice(String deviceClass) {
        MCADevice result = UNKNOWN;
        for (MCADevice mcaDevice : values()) {
            if (mcaDevice.deviceClass.equalsIgnoreCase(deviceClass)) {
                result = mcaDevice;
                break;
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return deviceClass;
    }

}