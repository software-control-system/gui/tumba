package fr.soleil.tumba.bean.mca.exception;

/**
 * An {@link Exception} that can be thrown when a problem occurred with a ROI file.
 * 
 * @author GIRARDOT
 */
public class RoiFileException extends Exception {

    private static final long serialVersionUID = 7827564613121613049L;

    public RoiFileException() {
        super();
    }

    public RoiFileException(String message) {
        super(message);
    }

    public RoiFileException(Throwable cause) {
        super(cause);
    }

    public RoiFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoiFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
