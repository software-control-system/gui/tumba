package fr.soleil.tumba.bean.mca.component;

import java.awt.event.ActionEvent;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;

import org.slf4j.Logger;

import fr.soleil.comete.swing.CheckBox;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.lib.project.swing.file.ExtensionFileFilter;
import fr.soleil.tumba.bean.mca.utils.IChannelCountManager;
import fr.soleil.tumba.bean.mca.utils.IChannelManager;
import fr.soleil.tumba.bean.mca.utils.IRoiManager;
import fr.soleil.tumba.bean.mca.utils.RoiListTarget;

public class SaveRoisButton extends ASetRoisButton {

    private static final long serialVersionUID = -6035017882735497938L;

    protected static final String NEW_LINE = "\n";

    protected ProgressDialog progressDialog;
    private final WeakReference<RoiListTarget> roiListTargetRef;
    private final Logger logger;

    public SaveRoisButton(IRoiManager roiManager, IChannelCountManager channelCountManager,
            IChannelManager channelManager, RoiListTarget roiListTarget, CheckBox allChannelROI, Logger logger) {
        super(roiManager, channelCountManager, channelManager, allChannelROI);
        this.roiListTargetRef = roiListTarget == null ? null : new WeakReference<>(roiListTarget);
        this.logger = logger;
    }

    @Override
    protected boolean isEmptyParamSupportedFor1Channel() {
        return false;
    }

    @Override
    protected boolean isEmptyParamSupportedForAllChannels() {
        return true;
    }

    @Override
    protected String buildParameterForAllChannels(String channelSeparator, int currentChannel,
            String... preparedParam) {
        String parameter;
        RoiListTarget roiListTarget = ObjectUtils.recoverObject(roiListTargetRef);
        if (roiListTarget == null) {
            parameter = ObjectUtils.EMPTY_STRING;
        } else {
            IChannelCountManager channelCountManager = ObjectUtils.recoverObject(channelCountManagerRef);
            int channelCount = channelCountManager == null ? 0 : channelCountManager.getChannelCount();
            StringBuilder param1 = new StringBuilder();
            for (int channelNumber = 0; channelNumber < channelCount; channelNumber++) {
                param1.append(Integer.toString(channelNumber));
                if (channelNumber == currentChannel) {
                    for (String p : preparedParam) {
                        param1.append(ROI_SEPARATOR).append(p);
                    }
                    param1.append(channelSeparator);
                } else {
                    int[] rois = roiListTarget.getRois(channelNumber);
                    if (rois != null) {
                        for (int p : rois) {
                            param1.append(ROI_SEPARATOR).append(p);
                        }
                    }
                    param1.append(channelSeparator);
                }
            }
            if (param1.length() > 0) {
                param1.delete(param1.length() - channelSeparator.length(), param1.length());
                parameter = param1.toString();
                param1.delete(0, param1.length());
            } else {
                parameter = ObjectUtils.EMPTY_STRING;
            }
        }
        return parameter;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        IRoiManager roiManager = ObjectUtils.recoverObject(roiManagerRef);
        RoiListTarget roiListTarget = ObjectUtils.recoverObject(roiListTargetRef);
        if ((roiManager != null) && (roiListTarget != null)) {
            JFileChooser roiChooser = roiManager.getRoiChooser();
            if (roiChooser != null) {
                if (roiChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                    File roiFile = roiChooser.getSelectedFile();
                    FileFilter filter = roiChooser.getFileFilter();
                    if (filter instanceof ExtensionFileFilter) {
                        ExtensionFileFilter extensionFileFilter = (ExtensionFileFilter) filter;
                        String extension = FileUtils.getExtension(roiFile);
                        if ((extension == null) || (!extension.equalsIgnoreCase(extensionFileFilter.getExtension()))) {
                            roiFile = new File(roiFile.getAbsolutePath() + '.' + extensionFileFilter.getExtension());
                            roiChooser.setSelectedFile(roiFile);
                        }
                    }
                    int confirmation = JOptionPane.YES_OPTION;
                    if (roiFile.exists()) {
                        confirmation = JOptionPane.showConfirmDialog(this,
                                roiFile.getName() + " already exists. Overwrite?", "Warning!",
                                JOptionPane.YES_NO_OPTION);
                    }
                    if (confirmation == JOptionPane.YES_OPTION) {
                        final File fileToWrite = roiFile;
                        if (progressDialog == null) {
                            progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(this));
                        }
                        progressDialog.setTitle("Saving ROIs to " + fileToWrite.getAbsolutePath());
                        progressDialog.setMainMessage(progressDialog.getTitle());
                        progressDialog.setProgressIndeterminate(true);
                        progressDialog.pack();
                        progressDialog.setLocationRelativeTo(this);
                        progressDialog.setVisible(true);
                        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                            @Override
                            protected Void doInBackground() throws Exception {
                                String toWrite = buildParameter(NEW_LINE);
                                roiManager.writeRoiFile(fileToWrite, toWrite);
                                return null;
                            }

                            @Override
                            protected void done() {
                                try {
                                    get();
                                } catch (InterruptedException e) {
                                    logger.warn("ROI saving interrupted");
                                } catch (ExecutionException e) {
                                    logger.error(e.getCause().getMessage(), e.getCause());
                                    roiManager.showMessageDialog(SaveRoisButton.this, e.getCause().getMessage(),
                                            "Error", JOptionPane.ERROR_MESSAGE);
                                } finally {
                                    progressDialog.setVisible(false);
                                }
                            }
                        };
                        worker.execute();
                    }
                }
            }
        }
    }

}