package fr.soleil.tumba.bean.mca.component;

import java.lang.ref.WeakReference;

import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.tumba.bean.mca.utils.ChannelConstants;
import fr.soleil.tumba.bean.mca.utils.IChannelCountManager;
import fr.soleil.tumba.bean.mca.utils.IChannelManager;
import fr.soleil.tumba.bean.mca.utils.IRoiManager;
import fr.soleil.tumba.bean.mca.utils.RoiConstants;

public abstract class ASetRoisButton extends StringButton implements RoiConstants, ChannelConstants {

    private static final long serialVersionUID = 2416029455621201946L;

    protected final WeakReference<IRoiManager> roiManagerRef;
    protected final WeakReference<IChannelCountManager> channelCountManagerRef;
    protected final WeakReference<IChannelManager> channelManagerRef;
    protected final WeakReference<CheckBox> allChannelROIRef;

    public ASetRoisButton(IRoiManager roiManager, IChannelCountManager channelCountManager,
            IChannelManager channelManager, CheckBox allChannelROI) {
        super();
        this.roiManagerRef = roiManager == null ? null : new WeakReference<>(roiManager);
        this.channelCountManagerRef = channelCountManager == null ? null : new WeakReference<>(channelCountManager);
        this.channelManagerRef = channelManager == null ? null : new WeakReference<>(channelManager);
        this.allChannelROIRef = allChannelROI == null ? null : new WeakReference<>(allChannelROI);
        setButtonLook(true);
    }

    protected void afterValidSetRoiPreparation() {
        // nothing to do by default
    }

    protected void actionOnInvalidCurrentChannel() {
        // nothing to do by default
    }

    protected abstract String buildParameterForAllChannels(String channelSeparator, int currentChannel,
            String... preparedParam);

    protected abstract boolean isEmptyParamSupportedFor1Channel();

    protected abstract boolean isEmptyParamSupportedForAllChannels();

    protected String buildParameterForCurrentChannel(String channelSeparator, int currentChannel,
            String... preparedParam) {
        StringBuilder param1 = new StringBuilder(Integer.toString(currentChannel));
        for (String p : preparedParam) {
            param1.append(ROI_SEPARATOR).append(p);
        }
        String parameter = param1.toString();
        param1.delete(0, param1.length());
        return parameter;
    }

    protected String buildParameter(String channelSeparator) {
        String parameter = null;
        IRoiManager roiManager = ObjectUtils.recoverObject(roiManagerRef);
        IChannelManager channelManager = ObjectUtils.recoverObject(channelManagerRef);
        CheckBox allChannelROI = ObjectUtils.recoverObject(allChannelROIRef);
        if ((roiManager != null) && (channelManager != null) && (allChannelROI != null)) {
            int currentChannel = channelManager.getCurrentChannel();
            String[] param = roiManager.prepareSetRoiButton();
            if (param != null) {
                if (allChannelROI.isSelected()) {
                    if ((param.length > 0) || isEmptyParamSupportedForAllChannels()) {
                        parameter = buildParameterForAllChannels(channelSeparator, currentChannel, param);
                    }
                } else if (currentChannel > -1) {
                    if ((param.length > 0) || isEmptyParamSupportedFor1Channel()) {
                        parameter = buildParameterForCurrentChannel(channelSeparator, currentChannel, param);
                    } else {
                        actionOnInvalidCurrentChannel();
                    }
                }
                afterValidSetRoiPreparation();
            }
        }
        return parameter;
    }

}
