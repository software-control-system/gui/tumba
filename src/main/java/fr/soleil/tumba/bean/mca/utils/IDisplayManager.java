package fr.soleil.tumba.bean.mca.utils;

import java.awt.Component;

public interface IDisplayManager {
    public void showMessageDialog(Component parentComponent, Object message, String title, int messageType);
}
