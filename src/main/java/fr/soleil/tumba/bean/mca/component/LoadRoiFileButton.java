package fr.soleil.tumba.bean.mca.component;

import java.awt.event.ActionEvent;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.slf4j.Logger;

import fr.soleil.comete.swing.StringButton;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.tumba.bean.mca.utils.IRoiManager;

/**
 * Button to load ROIs in tumba.
 * 
 * @author GIRARDOT
 */
public class LoadRoiFileButton extends StringButton {

    private static final long serialVersionUID = -3047945157054656512L;

    private final WeakReference<IRoiManager> roiManagerRef;
    private final Logger logger;

    protected ProgressDialog progressDialog;

    public LoadRoiFileButton(IRoiManager roiManager, Logger logger) {
        super();
        roiManagerRef = roiManager == null ? null : new WeakReference<>(roiManager);
        this.logger = logger;
        setButtonLook(true);
    }

    protected void doActionPerformed(ActionEvent e) {
        super.actionPerformed(e);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        setParameter(null);
        IRoiManager roiManager = ObjectUtils.recoverObject(roiManagerRef);
        if (roiManager != null) {
            JFileChooser roiChooser = roiManager.getRoiChooser();
            if (roiChooser != null) {
                if (roiChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                    File roiFile = roiChooser.getSelectedFile();
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(this));
                    }
                    progressDialog.setTitle("Loading " + roiFile.getAbsolutePath());
                    progressDialog.setMainMessage(progressDialog.getTitle());
                    progressDialog.setProgressIndeterminate(true);
                    progressDialog.pack();
                    progressDialog.setLocationRelativeTo(this);
                    progressDialog.setVisible(true);
                    SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
                        @Override
                        protected String doInBackground() throws Exception {
                            return roiManager.loadROIs(roiFile);
                        }

                        @Override
                        protected void done() {
                            try {
                                String rois = get();
                                setParameter(rois);
                                doActionPerformed(e);
                            } catch (InterruptedException e) {
                                logger.warn("ROI loading interrupted");
                            } catch (ExecutionException e) {
                                logger.error(e.getCause().getMessage(), e.getCause());
                                roiManager.showMessageDialog(LoadRoiFileButton.this, e.getCause().getMessage(), "Error",
                                        JOptionPane.ERROR_MESSAGE);
                            } finally {
                                roiManager.refreshDynamicRois();
                                progressDialog.setVisible(false);
                            }
                        }
                    };
                    worker.execute();
                }
            }
        }
    }
}