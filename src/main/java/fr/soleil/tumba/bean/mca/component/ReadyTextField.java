package fr.soleil.tumba.bean.mca.component;

import fr.soleil.comete.swing.TextField;

public class ReadyTextField extends TextField {

    private static final long serialVersionUID = -4585707529870606356L;

    private boolean isReadyToSend;

    public ReadyTextField() {
        super();
        isReadyToSend = false;
    }

    public void sendAction() {
        this.isReadyToSend = true;
        send();
    }

    @Override
    public void send() {
        if (isReadyToSend) {
            super.send();
            isReadyToSend = false;
        }
    }
}