package fr.soleil.tumba.bean.mca.utils;

/**
 * An interface for something able to manage some components visibility according to a {@link DeviceMode} and a device
 * state.
 * 
 * @author GIRARDOT
 */
public interface IModeAndStateManager {

    /**
     * Updates the components visibility and availability according to a {@link DeviceMode} and a device state.
     * 
     * @param mode The {@link DeviceMode}.
     * @param state The device state.
     */
    public void updateComponentsState(DeviceMode mode, String state);

    /**
     * Updates the components visibility according to a {@link DeviceMode}
     * 
     * @param mode The {@link DeviceMode}.
     */
    public void updateVisibility(DeviceMode mode);

    /**
     * Updates the connections according to a {@link DeviceMode}.
     * 
     * @param mode The {@link DeviceMode}.
     */
    public void updateConnections(DeviceMode mode);
}
