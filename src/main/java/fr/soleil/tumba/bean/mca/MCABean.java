package fr.soleil.tumba.bean.mca;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Collator;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.target.DeviceLogSpectrumAdapter;
import fr.soleil.comete.box.util.NumberReadingDelegate;
import fr.soleil.comete.box.util.StringArrayReadingDelegate;
import fr.soleil.comete.box.util.StringReadingDelegate;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.util.DefaultValueListCellRenderer;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.awt.ColorUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.lib.project.swing.ScrollMagnetism;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.file.ExtensionFileFilter;
import fr.soleil.lib.project.swing.text.ErrorTextField;
import fr.soleil.tumba.bean.mca.component.LoadRoiFileButton;
import fr.soleil.tumba.bean.mca.component.MemoryComboBox;
import fr.soleil.tumba.bean.mca.component.MultiRoiNonAttrNumberSpectrumViewer;
import fr.soleil.tumba.bean.mca.component.ReadyTextField;
import fr.soleil.tumba.bean.mca.component.SaveRoisButton;
import fr.soleil.tumba.bean.mca.component.SetRoisButton;
import fr.soleil.tumba.bean.mca.component.StreamCheckBox;
import fr.soleil.tumba.bean.mca.exception.RoiFileException;
import fr.soleil.tumba.bean.mca.utils.ChannelConstants;
import fr.soleil.tumba.bean.mca.utils.ChannelListener;
import fr.soleil.tumba.bean.mca.utils.CurrentModeTarget;
import fr.soleil.tumba.bean.mca.utils.DeviceMode;
import fr.soleil.tumba.bean.mca.utils.IChannelCountManager;
import fr.soleil.tumba.bean.mca.utils.IChannelManager;
import fr.soleil.tumba.bean.mca.utils.IMCADisplayManager;
import fr.soleil.tumba.bean.mca.utils.IModeAndStateManager;
import fr.soleil.tumba.bean.mca.utils.IRoiManager;
import fr.soleil.tumba.bean.mca.utils.MCAConstants;
import fr.soleil.tumba.bean.mca.utils.MCADevice;
import fr.soleil.tumba.bean.mca.utils.NbChannelsTarget;
import fr.soleil.tumba.bean.mca.utils.RoiConstants;
import fr.soleil.tumba.bean.mca.utils.RoiListTarget;
import fr.soleil.tumba.bean.mca.utils.RoiTarget;
import fr.soleil.tumba.bean.mca.utils.StateTarget;

/**
 * {@link AbstractTangoBox} used to monitor MCA devices. Also known as "Tumba".
 */
public class MCABean extends AbstractTangoBox implements MCAConstants, RoiConstants, ChannelConstants,
        IModeAndStateManager, IMCADisplayManager, IChannelCountManager, IChannelManager, IRoiManager {

    private static final long serialVersionUID = 6472706287249716163L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(MCABean.class);

    private static final ImageIcon RECORD_CONFIG_ICON = new ImageIcon(
            MCABean.class.getResource("/org/tango-project/tango-icon-theme/22x22/actions/document-properties.png"));
    private static final ImageIcon START_ICON = new ImageIcon(
            MCABean.class.getResource("/org/tango-project/tango-icon-theme/22x22/actions/media-playback-start.png"));
    private static final ImageIcon STOP_ICON = new ImageIcon(
            MCABean.class.getResource("/org/tango-project/tango-icon-theme/22x22/actions/media-playback-stop.png"));
    private static final ImageIcon LOAD_CONFIG_FILE_ICON = new ImageIcon(
            MCABean.class.getResource("/org/tango-project/tango-icon-theme/22x22/actions/document-open.png"));

    // Channels colors for MCA device, when displaying all channels in chart
    private static final CometeColor[] VIEW_COLORS = new CometeColor[16];
    static {
        ColorUtils colorRotationTool = new ColorUtils();
        for (int i = 0; i < VIEW_COLORS.length; i++) {
            VIEW_COLORS[i] = ColorTool.getCometeColor(colorRotationTool.getNextColor());
        }
    }

    // GUI texts
    private static final String ROIS = "ROIs";
    private static final String ROI_SPACE = "ROI ";
    private static final String ROI_SPECTRUM = "ROI Spectrum";
    private static final String TITLE_LOG = "Log";
    private static final String ZERO = "0";
    private static final String ALL = "all";
    private static final String CONTROLS = "Controls";
    private static final String INFO = "Info";
    private static final String ROIS_SETTINGS = "ROIs Settings";
    private static final String VISIBLE = "Visible";
    private static final String ROI_LABEl_TEXT = "ROI";
    private static final String START = "Start";
    private static final String END = "End";
    private static final String LABEL = "Label";
    private static final String ROIS_VALUES = "Rois Values";
    private static final String CHANNEL_TO_SET = "Channel to set: ";
    private static final String REFRESH = "Refresh";
    private static final String ALL_DEFINED = "All Defined";
    private static final String ALL_CHANNELS = "All Channels";
    private static final String SET_ROIS = "Set ROIs";
    private static final String LOAD_ROIS = "Load ROIs...";
    private static final String SAVE_ROIS = "Save ROIs...";
    private static final String TITLE_REMOVE_ROIS = "Remove ROIs";
    private static final String ADD_LINE = "Add Line";
    private static final String REMOVE_LINE = "Remove Line";
    private static final String DATA_SPECTRUM = "Data Spectrum";
    private static final String TITLE_FILE_GENERATION = "File Generation : ";
    private static final String TYPE = "Type : ";
    private static final String TARGET_PATH = "Target Path : ";
    private static final String TARGET_FILE = "Target File : ";
    private static final String NB_ACQ_PER_FILE = "Nb Acq Per File : ";
    private static final String OK = "OK";
    private static final String CANCEL = "Cancel";
    private static final String RECORDING_CONFIGURATION = "Recording Configuration";
    private static final String ABORT = "Abort";
    private static final String TITLE_LOAD_CONFIG_FILE = "Load Config File";
    private static final String TXT = "txt";

    // Error messages
    private static final String FAILED_TO_CONNECT_TO_DEVICE = "Failed to connect to MCA device";
    private static final String CONNECTION_ERROR = "Connection Error";
    private static final String IS_NOT_A_SUPPORTED_DEVICE_CLASS = " is not a supported device class!\nSupported device classes are: ";
    private static final String CLASS_NAME_CASE_IS_IMPORTANT = "\n(class name case is important)";
    private static final String COULD_NOT_CONNECT_TO_THE_FOLLOWING_ATTRIBUTES = "Could not connect to the following attributes (";
    private static final String COULD_NOT_CONNECT_TO_THE_FOLLOWING_COMMANDS = "Could not connect to the following commands (";
    private static final String CONNECTION_TITLE_SEPARATOR = "):\n";
    private static final String CONNECTION_LIST_START = "--------------------\n";
    private static final String CONNECTION_LIST_SEPARATOR = "\n";
    private static final String CONNECTION_LIST_END = "\n--------------------";
    private static final String COMMAND_LIST_SEPARATOR = "\n\n\n";
    private static final String IS_NOT_A_VALID_ROI_FILE = " is not a valid ROI file";
    private static final String CAN_T_DEFINE_ROIS_FOR_A_CHANNEL_WHEN_ALREADY_DEFINED_FOR_ALL_CHANNELS = " is not a valid ROI file:\nCan't define ROIs for a channel when already defined for all channels";
    private static final String CAN_T_DEFINE_ROIS_FOR_ALL_CHANNELS_WHEN_ALREADY_DEFINED_FOR_AT_LEAST_1_CHANNEL = " is not a valid ROI file:\nCan't define ROIs for all channels when already defined for at least 1 channel";
    private static final String ROI_END_MUST_BE_ROI_START = " is not a valid ROI file:\nROI end must be > ROI start (channel ";
    private static final String CLOSE_PARENTHESIS = ")";
    private static final String FAILED_TO_LOAD_ROIS_FROM = "Failed to load ROIs from ";
    private static final String CAN_T_SAVE_ROIS_IN_NULL_FILE = "Can't save ROIs in null file";
    private static final String NOTHING_TO_SAVE = "Nothing to save";
    private static final String FAILED_TO_SAVE_ROIS_IN = "Failed to save ROIs in ";

    // ROI texts
    private static final String ROI = "roi";
    private static final String HROI = "hroi";
    private static final String INDEX_SEPARATOR = "_";

    private static final Font TITLE_FONT = new Font(Font.DIALOG, Font.BOLD, 15);
    private static final CometeFont TITLE_COMETE_FONT = FontTool.getCometeFont(TITLE_FONT);
    private static final Font CHANNEL_TITLE_FONT = new Font(Font.DIALOG, Font.ITALIC | Font.BOLD, 12);
    private static final Font CHANNEL_INDEX_FONT = new Font(Font.DIALOG, Font.ITALIC | Font.PLAIN, 12);

    private static final Color VALUE_ERROR_COLOR = new Color(255, 200, 100);

    private static final Insets TITLE_MARGIN = new Insets(1, 5, 1, 5);
    private static final Insets VIEWER_MARGIN = new Insets(1, 0, 1, 5);
    private static final Insets SETTER_MARGIN = new Insets(1, 0, 1, 5);

    // Device name Values
    public static final Collection<String> SUPPORTED_DEVICE_CLASSES = Arrays.asList(XIA_DXP, DANTE_DPP);

    private static final int MAX_ROIS = 20;
    private int requestedROIs = 3;

    private final Collection<String> badAttributes;
    private final Collection<String> badCommands;

    private final RoiTarget roiTarget;

    private final Object roiLock, refreshLock;
    // ROI start and end are positive int values
    private int[] lastRoiValue;

    // Data
    private final MultiRoiNonAttrNumberSpectrumViewer dataViewer;

    // Controls
    private JPanel controlsPanel;
    private JScrollPane controlsSettingsScrollPane;
    private JPanel controlsSettingsPanel;
    private JPanel controlsActionPanel;
    private StringButton startCommandViewer;
    private StringButton loadConfigFileViewer;
    private ComboBox configListBox;
    private StringButton stopCommandViewer;
    private StringButton recordingConfigButton;
    private JDialog recordConfigDialog;
    private final NbChannelsTarget nbChannelsTarget;

    private JLabel[] controlAttributesLabels;
    private Label[] controlAttributesViewers;
    private IComponent[] controlAttributesSetters;

    private final CurrentModeTarget currentModeTarget;

    // Info
    private JScrollPane infoScrollPane;
    private JPanel infoPanel;
    private JLabel[] infoAttributesLabels;
    private Label[] infoAttributesViewers;

    // Recording Config
    private StreamCheckBox fileGenerationBox;
    private MemoryComboBox streamTypeBox;
    private ReadyTextField streamTargetPathField;
    private ReadyTextField streamTargetFileField;
    private ReadyTextField streamNbAcqPerFileField;

    // ROIs
    private JPanel roisPanel;
    private JPanel roisSettingsPanel;
    private JPanel roisActionPanel;
    private JLabel visibleTitleLabel;
    private JLabel roiTitleLabel;
    private JLabel startTitleLabel;
    private JLabel endTitleLabel;
    private JLabel labelTitleLabel;
    private JLabel roiValuesLabel;
    private ErrorTextField[] startTextFields;
    private ErrorTextField[] endTextFields;
    private StringButton[] hroiButtons;
    private CheckBox allChannelROI;
    private StringButton setROIsButtonViewer;
    private StringButton clearROIsButtonViewer;
    private StringButton loadROIsButtonViewer;
    private StringButton addROILineButton;
    private StringButton removeROILineButton;
    private StringButton saveROIsButtonViewer;
    private JLabel[] roiLabels;
    private Label[] roiScalarViewersValues;
    private ConstrainedCheckBox[] roiVisibilityBoxes;
    private JButton allDefinedVisibileButton;
    private JButton refreshROIsButton;

    private JFileChooser roiChooser;

    // General
    private StateTarget stateTarget;
    private JPanel dataRightPanel;
    protected JTextArea fakeDataTopArea;
    private JPanel roiRightPanel;
    private JTabbedPane roiTabbedPane;
    private LogViewer logPanel;
    private DeviceLogSpectrumAdapter logAdapter;
    private JSplitPane mainSplitPane;
    private JSplitPane dataSplitPane;
    private int[] roiSize;
    private int sizeSpectre;
    private boolean twoSpectre;
    private String[] calibrationSelectionStrings;
    private double calibrationParameterA;
    private double calibrationParameterB;
    private long numberChannels;

    // Amplifier Selection
    private JPanel speedRadioButtonPanel;
    private ComboBox comboCommandViewer;
    private JLabel comboCommandLabel;
    private JPanel comboCommandPanel;

    private MCADevice deviceType;

    // Calibration
    private CalibrationSettingsPanel calibrationSettingsPanel;

    // BeamLineEnergyDeviceName
    private String beamLineEnergyDeviceName;

    // // Periodic Table ElementsBean
    private JComboBox<Integer> roiIndexSelection;

    // Channels for XIA
    private volatile int channelCount;

    private int currentChannel;
    private JPanel selectionPanel;
    private JLabel currentChannelLabel;
    private Spinner currentChannelSetter;
    private final ChannelListener currentChannelListener;
    private final RoiListTarget roiListTarget;

    private JLabel channelIndexTitleLabel;
    private JLabel channelIndexLabel;
    private JLabel roiChannelTitleLabel;
    private JLabel roiChannelLabel;
    private ConstrainedCheckBox showAllChannelsInChartBox;

    private final NumberFormat channelFormat;
    private JLabel noDeviceLabel;

    private boolean showAllChannelsInChart;

    private final BooleanScalarBox booleanBox;
    private final NumberScalarBox numberBox;
    private final StringMatrixBox stringMatrixBox;
    private final ChartViewerBox chartBox;

    private final NumberReadingDelegate numberDelegate;
    private final StringReadingDelegate stringDelegate;
    private final StringArrayReadingDelegate stringArrayDelegate;

    /**
     * Default constructor
     */
    public MCABean() {
        this(true);
    }

    /**
     * This constructor is used by the getGUIBuilderInstance method to provide an instance of this
     * class which has not had it's GUI elements initialized (ie, initGUI is not called in this
     * constructor).
     */
    public MCABean(boolean initGUI) {
        super();
        roiLock = new Object();
        refreshLock = new Object();
        infoAttributesLabels = new JLabel[0];
        infoAttributesViewers = new Label[0];
        controlAttributesLabels = new JLabel[0];
        controlAttributesViewers = new Label[0];
        controlAttributesSetters = new IComponent[0];
        sizeSpectre = 0;
        twoSpectre = false;
        calibrationSelectionStrings = new String[2];
        calibrationParameterA = 0;
        calibrationParameterB = 0;
        numberChannels = 0;
        calibrationSettingsPanel = new CalibrationSettingsPanel();
        beamLineEnergyDeviceName = ObjectUtils.EMPTY_STRING;
        currentChannelListener = new ChannelListener(this);
        roiListTarget = new RoiListTarget(currentChannelListener);
        mainSplitPane = null;
        showAllChannelsInChart = false;
        channelFormat = NumberFormat.getIntegerInstance();
        channelFormat.setMinimumIntegerDigits(2);
        channelCount = 0;
        currentChannel = CURRENT_CHANNEL_ERROR;
        badAttributes = Collections.newSetFromMap(new ConcurrentHashMap<>());
        badCommands = Collections.newSetFromMap(new ConcurrentHashMap<>());
        stringMatrixBox = (StringMatrixBox) CometeBoxProvider.getCometeBox(StringMatrixBox.class);
        stringMatrixBox.setFirstInitAllowed(true);
        chartBox = (ChartViewerBox) CometeBoxProvider.getCometeBox(ChartViewerBox.class);
        booleanBox = (BooleanScalarBox) CometeBoxProvider.getCometeBox(BooleanScalarBox.class);
        numberBox = (NumberScalarBox) CometeBoxProvider.getCometeBox(NumberScalarBox.class);
        numberDelegate = new NumberReadingDelegate();
        stringDelegate = new StringReadingDelegate();
        stringArrayDelegate = new StringArrayReadingDelegate();
        dataViewer = new MultiRoiNonAttrNumberSpectrumViewer();
        dataViewer.setCometeBackground(CometeColor.WHITE);
        dataViewer.setHeader(DATA_SPECTRUM);
        dataViewer.setRoiColors(ROI_COLORS);
        nbChannelsTarget = new NbChannelsTarget(this);
        currentModeTarget = new CurrentModeTarget(this, this, refreshLock);
        roiTarget = new RoiTarget(this, this, currentModeTarget, dataViewer, roiLock);
        if (initGUI) {
            noDeviceLabel = new JLabel(NO_DEVICE_STRING);
            noDeviceLabel.setFont(new Font(Font.DIALOG, Font.BOLD | Font.ITALIC, 24));
            noDeviceLabel.setHorizontalAlignment(SwingConstants.CENTER);
            noDeviceLabel.setVerticalAlignment(SwingConstants.CENTER);
            noDeviceLabel.setForeground(Color.RED);
            setLayout(new BorderLayout());
            add(noDeviceLabel, BorderLayout.CENTER);
            getStatusPanel().setBorder(new LineBorder(Color.BLACK, 1));
        }
    }

    @Override
    protected void clearGUI() {
        clearModels();
        cleanTopPanel();
        displayBadDevice();
    }

    @Override
    protected void onConnectionError() {
        showMessageDialog(this, FAILED_TO_CONNECT_TO_DEVICE, CONNECTION_ERROR, JOptionPane.ERROR_MESSAGE);
    }

    protected <C extends Component> void setComponentsVisible(boolean visible,
            @SuppressWarnings("unchecked") C... components) {
        if (components != null) {
            for (C component : components) {
                if (component != null) {
                    component.setVisible(visible);
                }
            }
        }
    }

    @Override
    protected void refreshGUI() {
        deviceType = MCADevice.UNKNOWN;
        // Because the different MCA devices do not exactly have the same attributes and commands,
        // the application must check to which kind of MCA device it is connected.
        TangoKey key = new TangoKey();
        TangoKeyTool.registerDeviceClass(key, getModel());
        String deviceClass = extractSingleValue(key, stringDelegate, UNKNOWN_DEVICE, LOGGER);
        if (PUBLISHER_DEVICE.equals(deviceClass) || FACADE_DEVICE.equals(deviceClass)) {
            // "FacadeDevice" and "Publisher" are used for tests.
            // Maybe someone wants to simulate an MCA device to test the application.
            // Let's check it.
            key = new TangoKey();
            TangoKeyTool.registerDeviceProperty(key, getModel(), DEVICE_TYPE);
            TangoKeyTool.registerRefreshed(key, Boolean.FALSE);
            deviceClass = extractSingleValue(key, stringDelegate, deviceClass, LOGGER);
        }
        deviceType = MCADevice.parseMCADevice(deviceClass);
        if (deviceType == MCADevice.UNKNOWN) {
            StringBuilder builder = new StringBuilder(deviceClass);
            builder.append(IS_NOT_A_SUPPORTED_DEVICE_CLASS);
            for (String devClass : SUPPORTED_DEVICE_CLASSES) {
                builder.append(devClass).append(COMMA);
            }
            builder.delete(builder.length() - 2, builder.length());
            builder.append(CLASS_NAME_CASE_IS_IMPORTANT);
            String message = builder.toString();
            builder.delete(0, builder.length());
            LOGGER.error(message);
            showMessageDialog(this, message, CONNECTION_ERROR, JOptionPane.ERROR_MESSAGE);
        } else {
            key = new TangoKey();
            TangoKeyTool.registerAttribute(key, getModel(), NB_CHANNELS);
            TangoKeyTool.registerRefreshed(key, Boolean.FALSE);
            channelCount = extractSingleValue(key, numberDelegate, Integer.valueOf(0), LOGGER).intValue();
        }
        roiSize = new int[0];

        // Initialize components only once
        if (mainSplitPane == null) {
            initComponents();
        } else {
            updateControlComponents();
            updateInfoComponents();
        }
        currentModeTarget.reset();

        cleanTopPanel();
        clearModels();

        if (deviceType == MCADevice.UNKNOWN) {
            dataSplitPane.setRightComponent(fakeDataTopArea);
            displayBadDevice();
        } else {
            dataSplitPane.setRightComponent(dataRightPanel);
            visibleTitleLabel.setVisible(false);
            setComponentsVisible(false, roiScalarViewersValues);
            setComponentsVisible(false, hroiButtons);
            setComponentsVisible(false, roiVisibilityBoxes);
            labelTitleLabel.setVisible(false);
            initDefaultAttributes();
            initDefaultCommands();
            displayBadAttributesAndCommands();
            setModels();
        } // end if (deviceType == MCADevice.UNKNOWN) ... else

    }

    @Override
    public JFileChooser getRoiChooser() {
        if (roiChooser == null) {
            roiChooser = generateRoiChooser();
        }
        return roiChooser;
    }

    protected JFileChooser generateRoiChooser() {
        JFileChooser roiChooser = new JFileChooser();
        ExtensionFileFilter txtFilter = new ExtensionFileFilter(TXT);
        roiChooser.addChoosableFileFilter(txtFilter);
        roiChooser.setFileFilter(txtFilter);
        return roiChooser;
    }

    protected void initDefaultAttributes() {
        if (deviceType.isLoadConfigFileEnabled()) {
            checkAttributes(BOARD_TYPE, NB_CHANNELS, CURRENT_MODE, FILE_GENERATION, STREAM_TYPE, STREAM_TARGET_PATH,
                    STREAM_TARGET_FILE, STREAM_NB_ACQ_PER_FILE);
        } else {
            checkAttributes(BOARD_TYPE, NB_CHANNELS, CURRENT_MODE);
        }
    }

    protected void addDynamicAttributes(Collection<String> attrList) {
        for (int i = 0; i < channelCount; i++) {
            String formatedIndex = channelFormat.format(i);
            for (String attribute : deviceType.getInfoAttributes()) {
                attrList.add(attribute + formatedIndex);
            }
        }
    }

    protected void cleanControls() {
        if (controlAttributesViewers != null) {
            for (Label viewer : controlAttributesViewers) {
                cleanWidget(viewer);
            }
        }
        if (controlAttributesSetters != null) {
            for (IComponent setter : controlAttributesSetters) {
                cleanWidget(setter);
            }
        }
    }

    @Override
    public void updateVisibility(DeviceMode mode) {
        if (deviceType != null && (deviceType != MCADevice.UNKNOWN)) {
            roiTabbedPane.remove(roisPanel);
            int index = 0;
            boolean standby = STANDBY.equalsIgnoreCase(stateTarget.getText());
            for (String attribute : deviceType.getControlAttributes()) {
                if (deviceType.isAvailableInMode(attribute, mode)) {
                    controlAttributesLabels[index].setVisible(true);
                    controlAttributesViewers[index].setVisible(true);
                    if (deviceType.isWritableInMode(attribute, mode)) {
                        controlAttributesSetters[index].setVisible(standby);
                    }
                } else {
                    controlAttributesLabels[index].setVisible(false);
                    controlAttributesViewers[index].setVisible(false);
                    if (controlAttributesSetters[index] != null) {
                        controlAttributesSetters[index].setVisible(false);
                    }
                }
                index++;
            } // end for (String attribute : deviceType.getControlAttributes())
            updateComponentsState(mode, stateTarget.getText());
            controlsSettingsPanel.revalidate();
            controlsSettingsScrollPane.revalidate();
            controlsPanel.revalidate();
            if (deviceType.areRoisEnabledInMode(mode)) {
                roiTabbedPane.addTab(ROIS, roisPanel);
                roiTabbedPane.setSelectedComponent(roisPanel);
            }
        }
    }

    @Override
    public void updateConnections(DeviceMode mode) {
        if ((mode != null) && (deviceType != null) && (deviceType != MCADevice.UNKNOWN)) {
            badAttributes.clear();
            int index = 0;
            TangoKey key = null;

            // rois
            cleanRois();
            // info
            cleanDynamicModels();
            // record config
            cleanRecordConfig();

            // controls
            for (String attribute : deviceType.getControlAttributes()) {
                cleanWidget(controlAttributesViewers[index]);
                cleanWidget(controlAttributesSetters[index]);
                if (deviceType.isAvailableInMode(attribute, mode)) {
                    if (!checkAttribute(attribute)) {
                        badAttributes.add(attribute);
                    }
                    key = generateAttributeKey(attribute);
                    setWidgetModel(controlAttributesViewers[index], stringBox, key);
                    if (deviceType.isWritableInMode(attribute, mode)) {
                        Number[] stepConfiguration = deviceType.getStepConfiguration(attribute);
                        if (stepConfiguration == null) {
                            ITextTarget target = (ITextTarget) controlAttributesSetters[index];
                            stringBox.setColorEnabled(target, false);
                            setWidgetModel(target, stringBox, key);
                        } else {
                            INumberTarget target = (INumberTarget) controlAttributesSetters[index];
                            numberBox.setColorEnabled(target, false);
                            setWidgetModel(target, numberBox, key);
                        }
                    }
                }
                index++;
            } // end for (String attribute : deviceType.getControlAttributes())

            // record config
            connectRecordConfig();

            // rois
            if (deviceType.areRoisEnabledInMode(mode)) {
                checkCommands(SET_ROIS_FROM_LIST, GET_ROIS, REMOVE_ROIS);
                connectRois();
            }

            // dynamic
            setDynamicModels();
        } // end if ((mode != null) && (deviceType != null) && (deviceType != MCADevice.UNKNOWN))
    }

    @Override
    public int getChannelCount() {
        return channelCount;
    }

    @Override
    public void setChannelCount(int channelCount) {
        if (this.channelCount != channelCount) {
            this.channelCount = channelCount;
            synchronized (refreshLock) {
                if (channelCount == 0) {
                    currentChannel = CURRENT_CHANNEL_ERROR;
                } else if (currentChannel >= this.channelCount) {
                    currentChannel = this.channelCount - 1;
                } else if (currentChannel < 0) {
                    currentChannel = 0;
                }
                cleanDynamicModels();
                setDynamicModels();
            }
        }
    }

    @Override
    public int getCurrentChannel() {
        return currentChannel;
    }

    @Override
    public INumberTarget getChannelSetter() {
        return currentChannelSetter;
    }

    @Override
    public int[] getLastRoiValue() {
        return lastRoiValue;
    }

    @Override
    public void setLastRoiValue(int... lastRoiValue) {
        this.lastRoiValue = lastRoiValue;
    }

    protected void cleanRois() {
        cleanWidget(roiListTarget);
        cleanWidget(setROIsButtonViewer);
        cleanWidget(loadROIsButtonViewer);
        cleanWidget(removeROILineButton);
        cleanWidget(clearROIsButtonViewer);
        cleanRoiModels();
    }

    protected void connectRois() {
        TangoKey key = generateCommandKey(SET_ROIS_FROM_LIST);
        stringBox.setSettable(setROIsButtonViewer, false);
        stringBox.setSettable(loadROIsButtonViewer, false);
        stringBox.setSettable(removeROILineButton, false);
        setWidgetModel(setROIsButtonViewer, stringBox, key);
        setWidgetModel(loadROIsButtonViewer, stringBox, key);
        setWidgetModel(removeROILineButton, stringBox, key);
        key = generateCommandKey(REMOVE_ROIS);
        clearROIsButtonViewer.setVisible(true);
        setWidgetModel(clearROIsButtonViewer, stringBox, key);
        key = generateCommandKey(GET_ROIS);
        TangoKeyTool.registerRefreshed(key, Boolean.TRUE);
        setWidgetModel(roiListTarget, stringMatrixBox, key);
    }

    protected void initDefaultCommands() {
        if (deviceType.isLoadConfigFileEnabled()) {
            checkCommands(INIT, STATE, STATUS, SNAP, STOP, GET_CONFIGURATIONS_FILES_ALIAS, LOAD_CONFIG_FILE);
        } else {
            checkCommands(INIT, STATE, STATUS, SNAP, STOP);
        }
    }

    // Checks for bad attributes and fills badAttributes list
    @Override
    protected void checkAttributes(String... attributeShortNames) {
        badAttributes.clear();
        if (attributeShortNames != null) {
            for (String attributeShortName : attributeShortNames) {
                if (!checkAttribute(attributeShortName)) {
                    badAttributes.add(attributeShortName);
                }
            }
        }
    }

    // Checks for bad commands and fills badCommands list
    @Override
    protected void checkCommands(String... commandShortNames) {
        badCommands.clear();
        if (commandShortNames != null) {
            for (String commandShortName : commandShortNames) {
                if (!checkCommand(commandShortName)) {
                    badCommands.add(commandShortName);
                }
            }
        }
    }

    @Override
    public void displayBadAttributesAndCommands() {
        StringBuilder buffer = new StringBuilder();
        int j = 0;
        Collection<String> sorted = new TreeSet<>(Collator.getInstance());
        if (badAttributes.size() > 0) {
            sorted.addAll(badAttributes);
            buffer.append(COULD_NOT_CONNECT_TO_THE_FOLLOWING_ATTRIBUTES);
            buffer.append(getModel()).append(CONNECTION_TITLE_SEPARATOR);
            buffer.append(CONNECTION_LIST_START);
            for (String attribute : sorted) {
                buffer.append(attribute);
                j++;
                buffer.append(COMMA);
                if (j == 5) {
                    buffer.append(CONNECTION_LIST_SEPARATOR);
                    j = 0;
                }
            }
            int index = buffer.lastIndexOf(COMMA);
            buffer.replace(index, buffer.length(), ObjectUtils.EMPTY_STRING);
            buffer.append(CONNECTION_LIST_END);
        }
        sorted.clear();
        j = 0;
        if (badCommands.size() > 0) {
            sorted.addAll(badCommands);
            if (badAttributes.size() > 0) {
                buffer.append(COMMAND_LIST_SEPARATOR);
            }
            buffer.append(COULD_NOT_CONNECT_TO_THE_FOLLOWING_COMMANDS);
            buffer.append(getModel()).append(CONNECTION_TITLE_SEPARATOR);
            buffer.append(CONNECTION_LIST_START);
            for (String command : sorted) {
                buffer.append(command);
                j++;
                buffer.append(COMMA);
                if (j == 5) {
                    buffer.append(CONNECTION_LIST_SEPARATOR);
                    j = 0;
                }
            }
            int index = buffer.lastIndexOf(COMMA);
            buffer.replace(index, buffer.length(), ObjectUtils.EMPTY_STRING);
            buffer.append(CONNECTION_LIST_END);
        }
        sorted.clear();
        if (badAttributes.size() > 0 || badCommands.size() > 0) {
            showMessageDialog(this, buffer, CONNECTION_ERROR, JOptionPane.ERROR_MESSAGE);
        }
        buffer.delete(0, buffer.length());
    }

    /**
     * This is an alternative to {@link JOptionPane#showMessageDialog(Component, Object, String, int)}, with an always
     * on top dialog. This way, we are sure the dialog will always be visible, even in case of a Splash that could
     * normally hide it.
     * 
     * @param parentComponent The parent component
     * @param message The message to display
     * @param title the title
     * @param messageType the type of message
     * @see JOptionPane#showMessageDialog(Component, Object, String, int)
     */
    @Override
    public void showMessageDialog(Component parentComponent, Object message, String title, int messageType) {
        JOptionPane pane = new JOptionPane();
        pane.setMessageType(messageType);
        pane.setMessage(message);
        JDialog dialog = pane.createDialog(parentComponent, title);
        dialog.setAlwaysOnTop(true); // this is the most important part of the hack
        dialog.setVisible(true);
    }

    @Override
    public void channelChanged(double channelIndex) {
        if (Double.isNaN(channelIndex)) {
            currentChannel = CURRENT_CHANNEL_ERROR;
            synchronized (refreshLock) {
                cleanDynamicModels();
            }
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                roiChannelLabel.setText(ObjectUtils.EMPTY_STRING);
            });
        } else {
            int value = (int) Math.round(channelIndex);
            boolean canSetCurrentChannel;
            if (value < 0) {
                canSetCurrentChannel = false;
            } else {
                canSetCurrentChannel = true;
            }
            boolean allChannels = value < 0;
            if (canSetCurrentChannel) {
                currentChannel = value;
                synchronized (refreshLock) {
                    setDynamicModels();
                }
            }
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                roiChannelLabel.setText(allChannels ? ALL : channelFormat.format(value));
            });
        }
    }

    protected void cleanDynamicModels() {
        for (Label viewer : infoAttributesViewers) {
            cleanWidget(viewer);
        }
    }

    protected void setDynamicModels() {
        TangoKey key;
        if ((currentChannel > -1) && (currentChannel < channelCount)) {
            String channelIndex = channelFormat.format(currentChannel);
            refreshDynamicRois();
            // 1st, clean widgets
            cleanDynamicModels();
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                channelIndexLabel.setText(channelIndex);
            });
            // 2nd connect info attributes
            List<String> infoAttributes = deviceType.getInfoAttributes();
            int count = infoAttributes.size();
            for (int i = 0; i < count; i++) {
                key = generateAttributeKey(infoAttributes.get(i) + channelIndex);
                setWidgetModel(infoAttributesViewers[i], stringBox, key);
            }
            // 3rd connect chart
            setChartModel();
        }
    }

    protected void setChartModel() {
        TangoKey key;
        cleanWidget(dataViewer);
        dataViewer.setData(null);
        dataViewer.resetAll(false);
        if (isShowAllChannelsInChart()) {
            for (int i = 0; i < channelCount; i++) {
                key = generateAttributeKey(CHANNEL + channelFormat.format(i));
                String id = key.getInformationKey();
                TangoKeyTool.registerAttribute(key, id);
                dataViewer.setDataViewCometeColor(id, VIEW_COLORS[i % VIEW_COLORS.length]);
                dataViewer.setDataViewLineWidth(id, 2);
                setWidgetModel(dataViewer, chartBox, key);
            }
        } else if ((currentChannel > -1) && (currentChannel < channelCount)) {
            key = generateAttributeKey(CHANNEL + channelFormat.format(currentChannel));
            String id = key.getInformationKey();
            dataViewer.setDataViewCometeColor(id, CometeColor.RED);
            dataViewer.setDataViewLineWidth(id, 2);
            setWidgetModel(dataViewer, chartBox, key);
        }
    }

    @Override
    public void refreshDynamicRois() {
        if ((currentChannel > -1) && (currentChannel < channelCount)) {
            synchronized (roiLock) {
                int roiCount;
                int[] rois = roiListTarget.getRois(currentChannel);
                roiCount = (rois == null ? 0 : rois.length / 2);
                roiSize = new int[roiCount];
                for (int i = 0; i < roiCount; i++) {
                    roiSize[i] = i + 1;
                }
                if (rois == null) {
                    roiTarget.setFlatNumberMatrix(null, 0, 0);
                } else {
                    roiTarget.setFlatNumberMatrix(rois, rois.length, 1);
                }
            }
            reconnectRoiModels();
            refreshRoisTextFields();
            refreshRoiTable(currentModeTarget.getMode());
        }
    }

    protected void cleanCurrentMode() {
        cleanWidget(currentModeTarget);
    }

    protected void connectCurrentMode() {
        TangoKey key = generateAttributeKey(CURRENT_MODE);
        stringBox.setUseErrorText(currentModeTarget, true);
        stringBox.setSettable(currentModeTarget, true);
        setWidgetModel(currentModeTarget, stringBox, key);
    }

    protected void cleanNbChannels() {
        cleanWidget(nbChannelsTarget);
    }

    protected void connectNbChannels() {
        TangoKey key = generateAttributeKey(NB_CHANNELS);
        setWidgetModel(nbChannelsTarget, numberBox, key);
    }

    protected void clearModels() {
        // attributes and commands available at init
        currentChannelSetter.removeSpinnerListener(currentChannelListener);
        cleanWidget(currentChannelSetter);
        cleanWidget(currentChannelListener);
        cleanWidget(logAdapter);

        // record config
        cleanRecordConfig();

        // data
        cleanWidget(roiTarget);
        cleanWidget(dataViewer);
        if (dataViewer != null) {
            dataViewer.resetAll();
            dataViewer.setRoiColors(ROI_COLORS);
        }
        // info
        cleanDynamicModels();

        // controls
        cleanCurrentMode();
        cleanControls();
        cleanNbChannels();
        cleanWidget(loadConfigFileViewer);
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            configListBox.setValueList();
            configListBox.setDisplayedList();
        });
        cleanWidget(startCommandViewer);
        cleanWidget(stopCommandViewer);

        // general
        cleanStatusModel();

        // rois
        cleanRois();
    }

    protected void setModels() {
        TangoKey key;

        // log
        key = generateAttributeKey(LOG);
        setWidgetModel(logAdapter, stringMatrixBox, key);

        // controls
        currentModeTarget.setText(null);
        connectCurrentMode();
        connectNbChannels();
        if (deviceType.isLoadConfigFileEnabled()) {
            key = generateCommandKey(LOAD_CONFIG_FILE);
            stringBox.setUserEnabled(loadConfigFileViewer, true);
            setWidgetModel(loadConfigFileViewer, stringBox, key);
            // CONTROLGUI-331: avoid too long displayed values
            String[] list;
            String[] configs;
            key = generateCommandKey(GET_CONFIGURATIONS_FILES_ALIAS);
            configs = extractSingleValue(key, stringArrayDelegate, null, LOGGER);
            if (configs == null) {
                list = null;
            } else {
                list = new String[configs.length];
                int index = 0;
                for (Object value : configs) {
                    String displayed = null;
                    if (value != null) {
                        displayed = value.toString();
                        int pos = displayed.indexOf(';');
                        if (pos > 0) {
                            displayed = displayed.substring(0, pos);
                        } else {
                            displayed = null;
                        }
                    }
                    list[index++] = displayed;
                }
            }
            final Object[] valueList = configs;
            final String[] displayedList = list;
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                configListBox.setValueList(valueList);
                configListBox.setDisplayedList(displayedList);
            });
        }

        key = generateCommandKey(SNAP);
        stringBox.setSettable(startCommandViewer, false);
        setWidgetModel(startCommandViewer, stringBox, key);
        key = generateCommandKey(STOP);
        stringBox.setSettable(stopCommandViewer, false);
        setWidgetModel(stopCommandViewer, stringBox, key);

        // general
        setStatusModel();
        key = generateAttributeKey(STATE);
        setWidgetModel(stateTarget, stringBox, key);

        currentChannelSetter.addSpinnerListener(currentChannelListener);
        currentChannelSetter.setNumberValue(Long.valueOf(0));
        currentChannelListener.setNumberValue(currentChannelSetter.getNumberValue());
    }

    protected GridBagConstraints generateTitleLabelConstraints(int x, int y) {
        GridBagConstraints labelConstraints = new GridBagConstraints();
        labelConstraints.fill = GridBagConstraints.BOTH;
        labelConstraints.gridx = x;
        labelConstraints.gridy = y;
        labelConstraints.weightx = 0;
        labelConstraints.weighty = 0;
        labelConstraints.insets = TITLE_MARGIN;
        return labelConstraints;
    }

    protected GridBagConstraints generateViewerConstraints(int x, int y) {
        GridBagConstraints viewerConstraints = new GridBagConstraints();
        viewerConstraints.fill = GridBagConstraints.BOTH;
        viewerConstraints.gridx = x;
        viewerConstraints.gridy = y;
        viewerConstraints.weightx = 1;
        viewerConstraints.weighty = 0;
        viewerConstraints.insets = VIEWER_MARGIN;
        return viewerConstraints;
    }

    protected GridBagConstraints generateSetterConstraints(int x, int y) {
        GridBagConstraints setterConstraints = new GridBagConstraints();
        setterConstraints.fill = GridBagConstraints.BOTH;
        setterConstraints.gridx = x;
        setterConstraints.gridy = y;
        setterConstraints.weightx = 0;
        setterConstraints.weighty = 0;
        setterConstraints.insets = SETTER_MARGIN;
        return setterConstraints;
    }

    protected void initComponents() {
        removeAll();
        setLayout(new BorderLayout());

        // At this step, we already know device type

        // rois
        initRois();

        // Roi names
        initRoiNames();

        // Refresh Values
        refreshCurrentSelectionValues();

        // info
        initInfo();

        // controls
        initControls();

        // general
        stateTarget = new StateTarget(this, currentModeTarget);
        fakeDataTopArea = new JTextArea("No control to display", 30, 30);
        fakeDataTopArea.setEditable(false);
        fakeDataTopArea.setBorder(null);
        fakeDataTopArea.setOpaque(false);
        dataRightPanel = new JPanel();
        dataRightPanel.setLayout(new GridBagLayout());

        channelIndexTitleLabel = new JLabel("Selected channel:");
        channelIndexTitleLabel.setFont(CHANNEL_TITLE_FONT);
        channelIndexLabel = new JLabel();
        channelIndexLabel.setFont(CHANNEL_INDEX_FONT);
        currentChannelLabel = new JLabel("Channel:");
        int min = 0;

        selectionPanel = new JPanel(new GridBagLayout());
        currentChannelSetter = generateNumberSpinner(min, channelCount - 1, 1, Double.NaN);

        GridBagConstraints labelConstraints = new GridBagConstraints();
        labelConstraints.fill = GridBagConstraints.VERTICAL;
        labelConstraints.gridx = 0;
        labelConstraints.gridy = 0;
        labelConstraints.weightx = 0;
        labelConstraints.weighty = 1;
        labelConstraints.insets = new Insets(0, 5, 0, 2);
        selectionPanel.add(currentChannelLabel, labelConstraints);
        GridBagConstraints setterConstraints = new GridBagConstraints();
        setterConstraints.fill = GridBagConstraints.VERTICAL;
        setterConstraints.gridx = 2;
        setterConstraints.gridy = 0;
        setterConstraints.weightx = 0;
        setterConstraints.weighty = 1;
        setterConstraints.insets = new Insets(0, 0, 0, 30);
        selectionPanel.add(currentChannelSetter, setterConstraints);
        GridBagConstraints indexTitleLabelConstraints = new GridBagConstraints();
        indexTitleLabelConstraints.fill = GridBagConstraints.VERTICAL;
        indexTitleLabelConstraints.gridx = 3;
        indexTitleLabelConstraints.gridy = 0;
        indexTitleLabelConstraints.weightx = 0;
        indexTitleLabelConstraints.weighty = 1;
        indexTitleLabelConstraints.insets = new Insets(0, 0, 0, 2);
        selectionPanel.add(channelIndexTitleLabel, indexTitleLabelConstraints);
        GridBagConstraints indexLabelConstraints = new GridBagConstraints();
        indexLabelConstraints.fill = GridBagConstraints.BOTH;
        indexLabelConstraints.gridx = 4;
        indexLabelConstraints.gridy = 0;
        indexLabelConstraints.weightx = 1;
        indexLabelConstraints.weighty = 1;
        indexLabelConstraints.insets = new Insets(0, 0, 0, 30);
        selectionPanel.add(channelIndexLabel, indexLabelConstraints);

        GridBagConstraints currentSelectionConstraints = new GridBagConstraints();
        currentSelectionConstraints.fill = GridBagConstraints.BOTH;
        currentSelectionConstraints.gridx = 0;
        currentSelectionConstraints.gridy = 0;
        currentSelectionConstraints.weightx = 1;
        currentSelectionConstraints.weighty = 0;
        dataRightPanel.add(selectionPanel, currentSelectionConstraints);

        GridBagConstraints controlsConstraints = new GridBagConstraints();
        controlsConstraints.fill = GridBagConstraints.BOTH;
        controlsConstraints.gridx = 0;
        controlsConstraints.gridy = 1;
        controlsConstraints.weightx = 1;
        controlsConstraints.weighty = 1.0d / 3.0d;
        dataRightPanel.add(controlsPanel, controlsConstraints);

        GridBagConstraints infosConstraints = new GridBagConstraints();
        infosConstraints.fill = GridBagConstraints.BOTH;
        infosConstraints.gridx = 0;
        infosConstraints.gridy = 2;
        infosConstraints.weightx = 1;
        infosConstraints.weighty = 1.0d / 3.0d;
        dataRightPanel.add(infoScrollPane, infosConstraints);

        GridBagConstraints statusConstraints = new GridBagConstraints();
        statusConstraints.fill = GridBagConstraints.HORIZONTAL;
        statusConstraints.gridx = 0;
        statusConstraints.gridy = 5;
        statusConstraints.weightx = 1;
        statusConstraints.weighty = 0;
        dataRightPanel.add(getStatusPanel(), statusConstraints);

        roiRightPanel = new JPanel(new GridBagLayout());

        logPanel = new LogViewer(getClass().getName());
        logPanel.setScrollMagnetism(ScrollMagnetism.BOTTOM);
        logAdapter = new DeviceLogSpectrumAdapter(logPanel);
        roiTabbedPane = new JTabbedPane();

        roiTabbedPane.addTab(TITLE_LOG, logPanel);

        GridBagConstraints calibrationConstraints = new GridBagConstraints();
        calibrationConstraints.fill = GridBagConstraints.BOTH;
        calibrationConstraints.gridx = 0;
        calibrationConstraints.gridy = 0;
        calibrationConstraints.weightx = 1;
        calibrationConstraints.weighty = 0.5;
        roiRightPanel.add(roiTabbedPane, calibrationConstraints);

        dataSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        JPanel chartPanel = new JPanel(new BorderLayout());
        chartPanel.setMinimumSize(new Dimension(0, 0));
        chartPanel.setBackground(Color.WHITE);
        showAllChannelsInChartBox = new ConstrainedCheckBox("Display all channels");
        showAllChannelsInChartBox.setSelected(isShowAllChannelsInChart());
        showAllChannelsInChartBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setShowAllChannelsInChart(showAllChannelsInChartBox.isSelected());
            }
        });
        showAllChannelsInChartBox.setOpaque(false);
        chartPanel.add(showAllChannelsInChartBox, BorderLayout.NORTH);
        chartPanel.add(dataViewer, BorderLayout.CENTER);

        dataSplitPane.setLeftComponent(roiRightPanel);
        dataSplitPane.setRightComponent(fakeDataTopArea);
        dataSplitPane.setDividerSize(8);
        dataSplitPane.setDividerLocation(0.5);
        dataSplitPane.setResizeWeight(0);
        dataSplitPane.setOneTouchExpandable(true);

        mainSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        mainSplitPane.setTopComponent(dataSplitPane);
        mainSplitPane.setBottomComponent(chartPanel);
        mainSplitPane.setDividerSize(8);
        mainSplitPane.setDividerLocation(0.5);
        mainSplitPane.setResizeWeight(0);
        mainSplitPane.setOneTouchExpandable(true);

        add(mainSplitPane, BorderLayout.CENTER);
        revalidate();
        repaint();
    }

    protected void cleanTopPanel() {
        if (mainSplitPane != null) {
            hideDefault();
            hideBadDevice();
            roiTabbedPane.remove(roisPanel);
        }
    }

    protected void hideDefault() {
        dataSplitPane.remove(dataRightPanel);
    }

    protected void displayDefault() {
        dataSplitPane.setRightComponent(dataRightPanel);
    }

    protected void hideBadDevice() {
        dataSplitPane.remove(fakeDataTopArea);
    }

    protected void displayBadDevice() {
        dataSplitPane.setRightComponent(fakeDataTopArea);
    }

    protected void initRois() {
        roisPanel = new JPanel(new GridBagLayout());
        roisPanel.setBorder(new TitledBorder(ROIS_SETTINGS));

        roisSettingsPanel = new JPanel(new GridBagLayout());

        roisActionPanel = new JPanel(new GridBagLayout());

        visibleTitleLabel = new JLabel(VISIBLE, SwingConstants.CENTER);
        visibleTitleLabel.setBorder(new MatteBorder(1, 1, 1, 0, Color.BLACK));
        roiTitleLabel = new JLabel(ROI_LABEl_TEXT, SwingConstants.CENTER);
        roiTitleLabel.setBorder(new MatteBorder(1, 1, 1, 0, Color.BLACK));
        startTitleLabel = new JLabel(START, SwingConstants.CENTER);
        startTitleLabel.setBorder(new MatteBorder(1, 1, 1, 0, Color.BLACK));
        endTitleLabel = new JLabel(END, SwingConstants.CENTER);
        endTitleLabel.setBorder(new MatteBorder(1, 1, 1, 1, Color.BLACK));
        labelTitleLabel = new JLabel(LABEL, SwingConstants.CENTER);
        labelTitleLabel.setBorder(new MatteBorder(1, 1, 1, 1, Color.BLACK));

        roiValuesLabel = new JLabel(ROIS_VALUES, SwingConstants.CENTER);
        roiValuesLabel.setBorder(new MatteBorder(1, 0, 1, 1, Color.BLACK));

        roiChannelTitleLabel = new JLabel(CHANNEL_TO_SET);
        roiChannelTitleLabel.setFont(CHANNEL_TITLE_FONT);
        roiChannelLabel = new JLabel();
        roiChannelLabel.setFont(CHANNEL_INDEX_FONT);

        GridBagConstraints roiChannelTitleLabelConstraints = new GridBagConstraints();
        roiChannelTitleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        roiChannelTitleLabelConstraints.gridwidth = 2;
        roiChannelTitleLabelConstraints.gridx = 0;
        roiChannelTitleLabelConstraints.gridy = 0;
        roiChannelTitleLabelConstraints.weightx = 0;
        roiChannelTitleLabelConstraints.insets = new Insets(0, 0, 30, 0);
        roisSettingsPanel.add(roiChannelTitleLabel, roiChannelTitleLabelConstraints);
        GridBagConstraints roiChannelLabelConstraints = new GridBagConstraints();
        roiChannelLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        roiChannelLabelConstraints.gridx = 2;
        roiChannelLabelConstraints.gridy = 0;
        roiChannelLabelConstraints.weightx = 0;
        roiChannelLabelConstraints.insets = new Insets(0, 0, 30, 0);
        roisSettingsPanel.add(roiChannelLabel, roiChannelLabelConstraints);

        GridBagConstraints visibleConstraints = new GridBagConstraints();
        visibleConstraints.fill = GridBagConstraints.HORIZONTAL;
        visibleConstraints.gridx = 0;
        visibleConstraints.gridy = 1;
        visibleConstraints.weightx = 0;
        roisSettingsPanel.add(visibleTitleLabel, visibleConstraints);

        GridBagConstraints roiConstraints = new GridBagConstraints();
        roiConstraints.fill = GridBagConstraints.HORIZONTAL;
        roiConstraints.gridx = 1;
        roiConstraints.gridy = 1;
        roiConstraints.weightx = 0;
        roisSettingsPanel.add(roiTitleLabel, roiConstraints);

        GridBagConstraints startConstraints = new GridBagConstraints();
        startConstraints.fill = GridBagConstraints.HORIZONTAL;
        startConstraints.gridx = 2;
        startConstraints.gridy = 1;
        startConstraints.weightx = 1.0d / 3.0d;
        roisSettingsPanel.add(startTitleLabel, startConstraints);

        GridBagConstraints endConstraints = new GridBagConstraints();
        endConstraints.fill = GridBagConstraints.HORIZONTAL;
        endConstraints.gridx = 3;
        endConstraints.gridy = 1;
        endConstraints.weightx = 1.0d / 3.0d;
        roisSettingsPanel.add(endTitleLabel, endConstraints);

        GridBagConstraints roiValuesLabel2 = new GridBagConstraints();
        roiValuesLabel2.fill = GridBagConstraints.HORIZONTAL;
        roiValuesLabel2.gridx = 4;
        roiValuesLabel2.gridy = 1;
        roiValuesLabel2.weightx = 1.0d / 3.0d;
        roisSettingsPanel.add(roiValuesLabel, roiValuesLabel2);

        GridBagConstraints labelConstraints = new GridBagConstraints();
        labelConstraints.fill = GridBagConstraints.HORIZONTAL;
        labelConstraints.gridx = 5;
        labelConstraints.gridy = 1;
        labelConstraints.weightx = 1.0d / 3.0d;
        roisSettingsPanel.add(labelTitleLabel, labelConstraints);

        roiLabels = new JLabel[MAX_ROIS];
        roiScalarViewersValues = new Label[MAX_ROIS];
        roiVisibilityBoxes = new ConstrainedCheckBox[MAX_ROIS];

        refreshROIsButton = new JButton(REFRESH);
        refreshROIsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshRoisTextFields();
                refreshRoiTable(currentModeTarget.getMode());
            }
        });

        allDefinedVisibileButton = new JButton(ALL_DEFINED);
        allDefinedVisibileButton.setMargin(new Insets(0, 0, 0, 0));
        allDefinedVisibileButton.setSelected(true);
        allDefinedVisibileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (roiVisibilityBoxes != null) {
                    int[] lastRois = lastRoiValue;
                    for (int i = 0; i < roiSize.length; i++) {
                        if (roiVisibilityBoxes[i] != null) {
                            if ((lastRois != null) && (2 * i + 1 < lastRois.length)
                                    && (lastRois[(2 * i)] > 0 || lastRois[2 * i + 1] > 0)) {
                                roiVisibilityBoxes[i].setSelected(true);
                            } else {
                                roiVisibilityBoxes[i].setSelected(false);
                            }
                        }
                    }
                    for (int i = roiSize.length; i < MAX_ROIS; i++) {
                        if (roiVisibilityBoxes[i] != null) {
                            roiVisibilityBoxes[i].setSelected(false);
                        }
                    }
                }
            }
        });

        allChannelROI = new CheckBox();
        allChannelROI.setTrueLabel(ALL_CHANNELS);
        allChannelROI.setFalseLabel(ALL_CHANNELS);

        loadROIsButtonViewer = new LoadRoiFileButton(this, LOGGER);
        loadROIsButtonViewer.setEnabled(false);
        loadROIsButtonViewer.setText(LOAD_ROIS);

        saveROIsButtonViewer = new SaveRoisButton(this, this, this, roiListTarget, allChannelROI, LOGGER);
        saveROIsButtonViewer.setText(SAVE_ROIS);

        clearROIsButtonViewer = new StringButton() {

            private static final long serialVersionUID = 2691756910722602922L;

            @Override
            public void actionPerformed(ActionEvent event) {
                String channel;
                if (allChannelROI.isSelected()) {
                    channel = Integer.toString(-1);
                } else if (currentChannel > -1) {
                    channel = Integer.toString(currentChannel);
                } else {
                    channel = null;
                }
                setParameter(channel);
                try {
                    super.actionPerformed(event);
                } finally {
                    refreshDynamicRois();
                }
            }
        };
        clearROIsButtonViewer.setButtonLook(true);
        clearROIsButtonViewer.setEnabled(false);
        clearROIsButtonViewer.setText(TITLE_REMOVE_ROIS);

        setROIsButtonViewer = new SetRoisButton(this, this, this, allChannelROI, clearROIsButtonViewer);
        setROIsButtonViewer.setEnabled(false);
        setROIsButtonViewer.setText(SET_ROIS);

        startTextFields = new ErrorTextField[MAX_ROIS];
        endTextFields = new ErrorTextField[MAX_ROIS];
        hroiButtons = new StringButton[MAX_ROIS];

        for (int i = 0; i < MAX_ROIS; i++) {
            boolean visible = i < requestedROIs;
            Color roiColor = new Color(ROI_COLORS[i]);

            roiVisibilityBoxes[i] = new ConstrainedCheckBox();
            roiVisibilityBoxes[i].setBorder(new LineBorder(Color.RED, 5));
            roiVisibilityBoxes[i].setVisible(visible);
            roiVisibilityBoxes[i].setSelected(false);
            roiVisibilityBoxes[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setROIsButtonViewer.setEnabled(true);
                }
            });
            roiVisibilityBoxes[i].setBackground(roiColor);

            final int index = i;
            startTextFields[i] = new ErrorTextField(ZERO);
            startTextFields[i].setVisible(visible);
            startTextFields[i].setErrorColor(getBackground());
            final int startTextFieldsIndex = i;

            startTextFields[i].addKeyListener(new KeyListener() {

                @Override
                public void keyPressed(KeyEvent e) {
                    // nothing to do
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    try {
                        // ROI start and end are positive int values
                        Integer.parseInt(startTextFields[startTextFieldsIndex].getText());
                        startTextFields[startTextFieldsIndex].setInError(false);
                    } catch (NumberFormatException e1) {
                        startTextFields[startTextFieldsIndex].setErrorColor(VALUE_ERROR_COLOR);
                        startTextFields[startTextFieldsIndex].setInError(true);
                    }
                }

                @Override
                public void keyTyped(KeyEvent e) {
                    // nothing to do
                }

            });

            startTextFields[i].getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void changedUpdate(DocumentEvent e) {
                    enableSetRoi(startTextFields[index]);
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    enableSetRoi(startTextFields[index]);
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    enableSetRoi(startTextFields[index]);
                }
            });

            endTextFields[i] = new ErrorTextField(ZERO);
            endTextFields[i].setVisible(visible);
            endTextFields[i].setErrorColor(getBackground());
            final int endTextFieldsIndex = i;
            endTextFields[i].addKeyListener(new KeyListener() {

                @Override
                public void keyPressed(KeyEvent e) {
                    // nothing to do
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    try {
                        // ROI start and end are positive int values
                        Integer.parseInt(endTextFields[startTextFieldsIndex].getText());
                        endTextFields[endTextFieldsIndex].setInError(false);
                    } catch (NumberFormatException e1) {
                        endTextFields[endTextFieldsIndex].setErrorColor(VALUE_ERROR_COLOR);
                        endTextFields[endTextFieldsIndex].setInError(true);
                    }
                }

                @Override
                public void keyTyped(KeyEvent e) {
                    // nothing to do
                }
            });

            endTextFields[i].getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void changedUpdate(DocumentEvent e) {
                    enableSetRoi(endTextFields[index]);
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    enableSetRoi(endTextFields[index]);
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    enableSetRoi(endTextFields[index]);
                }
            });

            int roiIndex = i + 1;

            final String MCAModel = model;
            final int currentROI = i;

            hroiButtons[i] = new StringButton() {

                private static final long serialVersionUID = 8208711190543479039L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    if ((currentChannel > -1) && (currentChannel < channelCount)) {
                        Chart hroiChart = new Chart();
                        hroiChart.setManagementPanelVisible(false);
                        hroiChart.setFreezePanelVisible(false);
                        hroiChart.setPreferredSize(new Dimension(500, 500));
                        String hroiName = HROI + channelFormat.format(currentChannel) + INDEX_SEPARATOR
                                + channelFormat.format(currentROI + 1);
                        TangoKey key = new TangoKey();
                        TangoKeyTool.registerAttribute(key, MCAModel, hroiName);
                        setWidgetModel(hroiChart, chartBox, key);
                        showMessageDialog(this, hroiChart, hroiName, JOptionPane.PLAIN_MESSAGE);
                        cleanWidget(hroiChart);
                    }
                }
            };
            hroiButtons[i].setVisible(visible);
            hroiButtons[i].setText(ROI_SPECTRUM);

            roiLabels[i] = new JLabel(ROI_SPACE + roiIndex);
            roiLabels[i].setVisible(visible);
            roiLabels[i].setBackground(roiColor);
            roiLabels[i].setOpaque(true);

            GridBagConstraints visibleConstraints2 = new GridBagConstraints();
            visibleConstraints2.fill = GridBagConstraints.HORIZONTAL;
            visibleConstraints2.gridx = 0;
            visibleConstraints2.gridy = i + 2;
            visibleConstraints2.weightx = 0;
            roisSettingsPanel.add(roiVisibilityBoxes[i], visibleConstraints2);

            GridBagConstraints roiConstraints2 = new GridBagConstraints();
            roiConstraints2.fill = GridBagConstraints.BOTH;
            roiConstraints2.gridx = 1;
            roiConstraints2.gridy = i + 2;
            roiConstraints2.weightx = 0;
            roisSettingsPanel.add(roiLabels[i], roiConstraints2);

            GridBagConstraints startConstraints2 = new GridBagConstraints();
            startConstraints2.fill = GridBagConstraints.BOTH;
            startConstraints2.gridx = 2;
            startConstraints2.gridy = i + 2;
            startConstraints2.weightx = 1.0d / 3.0d;
            roisSettingsPanel.add(startTextFields[i], startConstraints2);

            GridBagConstraints endConstraints2 = new GridBagConstraints();
            endConstraints2.fill = GridBagConstraints.BOTH;
            endConstraints2.gridx = 3;
            endConstraints2.gridy = i + 2;
            endConstraints2.weightx = 1.0d / 3.0d;
            roisSettingsPanel.add(endTextFields[i], endConstraints2);

            GridBagConstraints roiValuesTextFields = new GridBagConstraints();
            roiValuesTextFields.fill = GridBagConstraints.BOTH;
            roiValuesTextFields.gridx = 4;
            roiValuesTextFields.gridy = i + 2;
            roiValuesTextFields.weightx = 1.0d / 3.0d;
            roiScalarViewersValues[i] = new Label();
            roiScalarViewersValues[i].setVisible(visible);
            roiScalarViewersValues[i].setOpaque(true);
            roiScalarViewersValues[i].setCometeFont(TITLE_COMETE_FONT);
            roiScalarViewersValues[i].setCometeBackground(ColorTool.getCometeColor(roiColor));
            roiScalarViewersValues[i].setCometeFont(FontTool.getCometeFont(new Font(Font.DIALOG, Font.BOLD, 15)));
            roisSettingsPanel.add(roiScalarViewersValues[i], roiValuesTextFields);

            roiColor = null;

            GridBagConstraints hroiConstraints2 = new GridBagConstraints();
            hroiConstraints2.fill = GridBagConstraints.NONE;
            hroiConstraints2.anchor = GridBagConstraints.WEST;
            hroiConstraints2.gridx = 6;
            hroiConstraints2.gridy = i + 2;
            hroiConstraints2.weightx = 0;
            hroiConstraints2.insets = new Insets(0, 5, 0, 5);
            roisSettingsPanel.add(hroiButtons[i], hroiConstraints2);
        }

        int x = 0;

        GridBagConstraints setrefrechROIsButton = new GridBagConstraints();
        setrefrechROIsButton.fill = GridBagConstraints.NONE;
        setrefrechROIsButton.gridx = x++;
        setrefrechROIsButton.gridy = 0;
        setrefrechROIsButton.weightx = 0;
        roisActionPanel.add(refreshROIsButton, setrefrechROIsButton);

        GridBagConstraints glue1Constraints = new GridBagConstraints();
        glue1Constraints.fill = GridBagConstraints.BOTH;
        glue1Constraints.gridx = x++;
        glue1Constraints.gridy = 0;
        glue1Constraints.weightx = 0.5;
        roisActionPanel.add(Box.createGlue(), glue1Constraints);

        GridBagConstraints glue2Constraints = new GridBagConstraints();
        glue2Constraints.fill = GridBagConstraints.BOTH;
        glue2Constraints.gridx = x++;
        glue2Constraints.gridy = 0;
        glue2Constraints.weightx = 0.5;
        roisActionPanel.add(Box.createGlue(), glue2Constraints);

        GridBagConstraints checkboxConstraints = new GridBagConstraints();
        checkboxConstraints.fill = GridBagConstraints.NONE;
        checkboxConstraints.gridx = x++;
        checkboxConstraints.gridy = 0;
        checkboxConstraints.weightx = 0;
        roisActionPanel.add(allChannelROI, checkboxConstraints);

        addROILineButton = new StringButton() {

            private static final long serialVersionUID = 4376222788601355948L;

            @Override
            public void actionPerformed(ActionEvent event) {
                if (requestedROIs < MAX_ROIS) {
                    roiLabels[requestedROIs].setVisible(true);
                    startTextFields[requestedROIs].setVisible(true);
                    endTextFields[requestedROIs].setVisible(true);
                    DeviceMode mode = currentModeTarget.getMode();
                    if (deviceType.areRoisEnabledInMode(mode)) {
                        if (deviceType.isSpecialRoiMode(mode)) {
                            hroiButtons[requestedROIs].setVisible(true);
                        } else {
                            roiScalarViewersValues[requestedROIs].setVisible(true);
                        }
                    }
                    requestedROIs++;
                }
            }
        };
        addROILineButton.setText(ADD_LINE);
        GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.fill = GridBagConstraints.NONE;
        buttonConstraints.gridx = x++;
        buttonConstraints.gridy = 0;
        buttonConstraints.weightx = 0;
        roisActionPanel.add(addROILineButton, buttonConstraints);

        removeROILineButton = new StringButton() {

            private static final long serialVersionUID = 2334542620115841082L;

            @Override
            public void actionPerformed(ActionEvent event) {
                if (requestedROIs > 1) {
                    roiLabels[requestedROIs - 1].setVisible(false);
                    startTextFields[requestedROIs - 1].setVisible(false);
                    endTextFields[requestedROIs - 1].setVisible(false);
                    DeviceMode mode = currentModeTarget.getMode();
                    if (deviceType.areRoisEnabledInMode(mode)) {
                        if (deviceType.isSpecialRoiMode(mode)) {
                            hroiButtons[requestedROIs - 1].setVisible(false);
                        } else {
                            roiScalarViewersValues[requestedROIs - 1].setVisible(false);
                        }
                    }
                    requestedROIs--;
                }
                StringBuilder param = new StringBuilder();
                int[] rois;
                for (int i = 0; i < channelCount; i++) {
                    rois = roiListTarget.getRois(i);
                    if ((rois != null) && (rois.length > 2 * requestedROIs)) {
                        if (param.length() > 0) {
                            param.append(LIST_SEPARATOR);
                        }
                        param.append(i);
                        if (rois.length <= 2) {
                            param.append(ROI_SEPARATOR);
                        } else {
                            for (int j = 0; j < rois.length - 2; j++) {
                                param.append(ROI_SEPARATOR).append(rois[j]);
                            }
                        }
                    }
                } // end for (int i = 0; i < channelCount; i++)
                if (param.length() > 0) {
                    setParameter(param.toString());
                    param.delete(0, param.length());
                    super.actionPerformed(event);
                }
            }
        };
        removeROILineButton.setText(REMOVE_LINE);
        buttonConstraints.gridx = x++;
        roisActionPanel.add(removeROILineButton, buttonConstraints);

        GridBagConstraints setConstraints = new GridBagConstraints();
        setConstraints.fill = GridBagConstraints.NONE;
        setConstraints.gridx = x++;
        setConstraints.gridy = 0;
        setConstraints.weightx = 0;
        setConstraints.insets = new Insets(0, 20, 0, 0);
        roisActionPanel.add(setROIsButtonViewer, setConstraints);

        GridBagConstraints clearConstraints = new GridBagConstraints();
        clearConstraints.fill = GridBagConstraints.NONE;
        clearConstraints.gridx = x++;
        clearConstraints.gridy = 0;
        clearConstraints.weightx = 0;
        clearConstraints.insets = new Insets(0, 20, 0, 0);
        roisActionPanel.add(clearROIsButtonViewer, clearConstraints);

        GridBagConstraints loadConstraints = new GridBagConstraints();
        loadConstraints.fill = GridBagConstraints.NONE;
        loadConstraints.gridx = x++;
        loadConstraints.gridy = 0;
        loadConstraints.weightx = 0;
        loadConstraints.insets = new Insets(0, 20, 0, 0);
        roisActionPanel.add(loadROIsButtonViewer, loadConstraints);

        GridBagConstraints saveConstraints = new GridBagConstraints();
        saveConstraints.fill = GridBagConstraints.NONE;
        saveConstraints.gridx = x++;
        saveConstraints.gridy = 0;
        saveConstraints.weightx = 0;
        roisActionPanel.add(saveROIsButtonViewer, saveConstraints);

        GridBagConstraints glue3Constraints = new GridBagConstraints();
        glue3Constraints.fill = GridBagConstraints.BOTH;
        glue3Constraints.gridx = x++;
        glue3Constraints.gridy = 0;
        glue3Constraints.weightx = 0.5;
        roisActionPanel.add(Box.createGlue(), glue3Constraints);

        GridBagConstraints glue5Constraints = new GridBagConstraints();
        glue5Constraints.fill = GridBagConstraints.BOTH;
        glue5Constraints.gridx = x++;
        glue5Constraints.gridy = 0;
        glue5Constraints.weightx = 0.5;
        roisActionPanel.add(Box.createGlue(), glue5Constraints);

        GridBagConstraints glue4Constraints = new GridBagConstraints();
        glue4Constraints.fill = GridBagConstraints.BOTH;
        glue4Constraints.gridx = x++;
        glue4Constraints.gridy = 0;
        glue4Constraints.weightx = 0.5;
        roisActionPanel.add(Box.createGlue(), glue4Constraints);

        GridBagConstraints roiSettingConstraints = new GridBagConstraints();
        roiSettingConstraints.fill = GridBagConstraints.BOTH;
        roiSettingConstraints.gridx = 0;
        roiSettingConstraints.gridy = 0;
        roiSettingConstraints.weightx = 1;
        roiSettingConstraints.weighty = 1;
        roisPanel.add(new JScrollPane(roisSettingsPanel), roiSettingConstraints);

        JScrollPane roisActionScrollPane = new JScrollPane(roisActionPanel);
        roisActionScrollPane.setBorder(null);
        int policy = roisActionScrollPane.getHorizontalScrollBarPolicy();
        roisActionScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        Dimension size = roisActionScrollPane.getPreferredSize();
        roisActionScrollPane.setHorizontalScrollBarPolicy(policy);
        roisActionScrollPane.setPreferredSize(size);
        roisActionScrollPane.setMinimumSize(new Dimension(0, size.height));
        GridBagConstraints roiActionConstraints = new GridBagConstraints();
        roiActionConstraints.fill = GridBagConstraints.HORIZONTAL;
        roiActionConstraints.gridx = 0;
        roiActionConstraints.gridy = 1;
        roiActionConstraints.weightx = 1;
        roiActionConstraints.weighty = 0;
        roisPanel.add(roisActionScrollPane, roiActionConstraints);

        roiVisibilityBoxes[0].setSelected(true);
    }

    protected void initRoiNames() {
        for (int i = 0; i < MAX_ROIS; i++) {
            String roiName = MultiRoiNonAttrNumberSpectrumViewer.ROI_PREFIX + (i + 1);
            dataViewer.setRoiName(roiName, i);
        }
    }

    public void refreshCurrentSelectionValues() {

        if (sizeSpectre == 0) {
            calibrationSelectionStrings[0] = ZERO;
            sizeSpectre++;
        }

        if (sizeSpectre == 1 && twoSpectre) {
            calibrationSelectionStrings[1] = ZERO;
            sizeSpectre++;
        }

        if (sizeSpectre == 2) {
            twoSpectre = false;
            sizeSpectre = 0;
        }

        if (sizeSpectre < 2 && sizeSpectre != 0) {
            twoSpectre = true;
        }
        calibrationSettingsPanel.refreshGUI(calibrationSelectionStrings);
    }

    /**
     *
     */
    protected void initInfo() {
        infoPanel = new JPanel(new GridBagLayout());
        infoScrollPane = new JScrollPane(infoPanel);
        infoScrollPane.setBorder(new TitledBorder(INFO));
        updateInfoComponents();
    }

    protected void updateInfoComponents() {
        int y = infoAttributesLabels.length;

        List<String> infoAttributes = deviceType.getInfoAttributes();
        int count = infoAttributes.size();
        if (count != y) {
            JLabel[] newLabels = Arrays.copyOf(infoAttributesLabels, count);
            Label[] newViewers = Arrays.copyOf(infoAttributesViewers, count);
            if (count < y) {
                for (int i = count; i < y; i++) {
                    infoPanel.remove(infoAttributesLabels[i]);
                    infoPanel.remove(infoAttributesViewers[i]);
                    cleanWidget(infoAttributesViewers[i]);
                }
            } else {
                for (int i = 0; i < y; i++) {
                    newLabels[i].setText(infoAttributes.get(i));
                    newLabels[i].revalidate();
                }
                while (y < count) {
                    newLabels[y] = generateTitleLabel(deviceType.getAttributeTitle(infoAttributes.get(y)));
                    newViewers[y] = generateLabel();
                    newViewers[y].setCometeFont(TITLE_COMETE_FONT);
                    GridBagConstraints labelConstraints = generateTitleLabelConstraints(0, y);
                    GridBagConstraints viewerConstraints = generateViewerConstraints(1, y);
                    infoPanel.add(newLabels[y], labelConstraints);
                    infoPanel.add(newViewers[y++], viewerConstraints);
                }
            }
            infoAttributesLabels = newLabels;
            infoAttributesViewers = newViewers;
            infoPanel.revalidate();
        }
    }

    protected void initRecordConfig() {
        final JPanel recordConfigPanel = new JPanel(new GridBagLayout());

        int y = 0;

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 0;
        constraints.weighty = 1;
        constraints.gridx = 0;
        constraints.gridy = y++;
        constraints.insets = new Insets(0, 5, 0, 5);
        JLabel fileGenerationLabel = new JLabel(TITLE_FILE_GENERATION);
        recordConfigPanel.add(fileGenerationLabel, constraints);

        constraints.gridy = y++;
        JLabel streamTypeLabel = new JLabel(TYPE);
        recordConfigPanel.add(streamTypeLabel, constraints);

        constraints.gridy = y++;
        JLabel streamTargetPathLabel = new JLabel(TARGET_PATH);
        recordConfigPanel.add(streamTargetPathLabel, constraints);

        constraints.gridy = y++;
        JLabel streamTargetFileLabel = new JLabel(TARGET_FILE);
        recordConfigPanel.add(streamTargetFileLabel, constraints);

        constraints.gridy = y++;
        JLabel streamNbAcqPerFileLabel = new JLabel(NB_ACQ_PER_FILE);
        recordConfigPanel.add(streamNbAcqPerFileLabel, constraints);

        streamTypeBox = new MemoryComboBox();
        streamTargetPathField = new ReadyTextField();
        streamTargetFileField = new ReadyTextField();
        streamNbAcqPerFileField = new ReadyTextField();
        fileGenerationBox = new StreamCheckBox(streamTypeBox, streamTargetPathField, streamTargetFileField,
                streamNbAcqPerFileField, stringBox);

        y = 0;
        constraints.weightx = 1;
        constraints.gridx = 1;
        constraints.gridy = y++;
        recordConfigPanel.add(fileGenerationBox, constraints);

        constraints.gridy = y++;
        streamTypeBox.setValueList(CSV_STREAM, NEXUS_STREAM, LOG_STREAM);
        recordConfigPanel.add(streamTypeBox, constraints);

        constraints.gridy = y++;
        recordConfigPanel.add(streamTargetPathField, constraints);

        constraints.gridy = y++;
        recordConfigPanel.add(streamTargetFileField, constraints);

        constraints.gridy = y++;
        recordConfigPanel.add(streamNbAcqPerFileField, constraints);

        fileGenerationBox.updateSelection(false);

        constraints.gridy = y++;
        JPanel buttonPanel = new JPanel();

        JButton okButton = new JButton(OK);
        okButton.addActionListener((e) -> {
            if (recordConfigDialog != null) {
                recordConfigDialog.dispose();
            }
            fileGenerationBox.sendAction();
            if (fileGenerationBox.isSelected()) {
                streamTypeBox.sendAction();
                streamTargetPathField.sendAction();
                streamTargetFileField.sendAction();
                streamNbAcqPerFileField.sendAction();
            }
        });
        buttonPanel.add(okButton, constraints);
        JButton cancelButton = new JButton(CANCEL);
        cancelButton.addActionListener((e) -> {
            if (recordConfigDialog != null) {
                recordConfigDialog.dispose();
            }
            fileGenerationBox.cancel();
            streamTypeBox.cancel();
            streamTargetPathField.cancel();
            streamTargetFileField.cancel();
            streamNbAcqPerFileField.cancel();
        });
        buttonPanel.add(cancelButton, constraints);
        recordConfigPanel.add(buttonPanel, constraints);
        recordConfigPanel.setMinimumSize(new Dimension(600, 300));

        fileGenerationBox.setOpaque(false);
        streamTypeBox.setOpaque(false);
        stringBox.setColorEnabled(streamTargetPathField, false);
        stringBox.setColorEnabled(streamTargetFileField, false);
        stringBox.setColorEnabled(streamNbAcqPerFileField, false);

        recordingConfigButton = new StringButton();
        recordingConfigButton.setText("Recording Config");
        recordingConfigButton.setIcon(RECORD_CONFIG_ICON);
        final Object dialogLock = new Object();
        recordingConfigButton.addActionListener((e) -> {
            if (recordConfigDialog == null) {
                synchronized (dialogLock) {
                    if (recordConfigDialog == null) {
                        recordConfigDialog = new JDialog(WindowSwingUtils.getWindowForComponent(recordingConfigButton),
                                RECORDING_CONFIGURATION);
                        recordConfigDialog.setContentPane(recordConfigPanel);
                    }
                }
            }
            if (!recordConfigDialog.isVisible()) {
                recordConfigDialog.pack();
                Dimension size = recordConfigDialog.getSize(), minSize = recordConfigDialog.getMinimumSize();
                size.width = Math.max(size.width, minSize.width);
                recordConfigDialog.setSize(size);
                recordConfigDialog.setLocationRelativeTo(recordingConfigButton);
                recordConfigDialog.setVisible(true);
            }
        });
        recordingConfigButton.setEnabled(false);
    }

    protected void initControls() {
        initRecordConfig();
        controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridBagLayout());
        controlsActionPanel = new JPanel(new GridBagLayout());
        controlsSettingsPanel = new JPanel(new GridBagLayout());
        controlsSettingsScrollPane = new JScrollPane(controlsSettingsPanel);
        controlsSettingsScrollPane.setBorder(null);

        startCommandViewer = new StringButton();
        startCommandViewer.setText(START);

        ((JButton) startCommandViewer).setIcon(START_ICON);

        stopCommandViewer = new StringButton();
        stopCommandViewer.setText(ABORT);
        stopCommandViewer.setIcon(STOP_ICON);
        loadConfigFileViewer = new StringButton() {

            private static final long serialVersionUID = -679641383339305170L;

            @Override
            public void actionPerformed(ActionEvent e) {
                String parameter = getParameter();
                if (parameter != null && parameter.contains(ROI_SEPARATOR)) {
                    setParameter(parameter.substring(0, parameter.indexOf(ROI_SEPARATOR)));
                }
                super.actionPerformed(e);
                new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        if (currentModeTarget.getMode() == DeviceMode.UNKNOWN) {
                            // Force reconnecting to current mode and nb channels, as they were probably unreadable
                            // (don't wait retry time)
                            cleanCurrentMode();
                            cleanNbChannels();
                            connectCurrentMode();
                            connectNbChannels();
                        }
                        return null;
                    }
                }.execute();
            }
        };
        loadConfigFileViewer.setText(TITLE_LOAD_CONFIG_FILE);
        loadConfigFileViewer.setIcon(LOAD_CONFIG_FILE_ICON);

        GridBagConstraints startConstraints2 = new GridBagConstraints();
        startConstraints2.fill = GridBagConstraints.HORIZONTAL;
        startConstraints2.gridx = 0;
        startConstraints2.gridy = 0;
        startConstraints2.weightx = 1.0 / 5.0;
        startConstraints2.insets = new Insets(0, 5, 0, 5);
        GridBagConstraints stopConstraints = new GridBagConstraints();
        stopConstraints.fill = GridBagConstraints.HORIZONTAL;
        stopConstraints.gridx = 1;
        stopConstraints.gridy = 0;
        stopConstraints.weightx = 1.0 / 5.0;
        stopConstraints.insets = new Insets(0, 5, 0, 5);
        GridBagConstraints configConstraints = new GridBagConstraints();
        configConstraints.fill = GridBagConstraints.HORIZONTAL;
        configConstraints.gridx = 2;
        configConstraints.gridy = 0;
        configConstraints.weightx = 1.0 / 5.0;
        configConstraints.insets = new Insets(0, 5, 0, 5);
        GridBagConstraints clearDataConstraints = new GridBagConstraints();
        clearDataConstraints.fill = GridBagConstraints.HORIZONTAL;
        clearDataConstraints.gridx = 3;
        clearDataConstraints.gridy = 0;
        clearDataConstraints.weightx = 1.0 / 5.0;
        clearDataConstraints.insets = new Insets(0, 5, 0, 5);
        GridBagConstraints comboDataConstraints = new GridBagConstraints();
        comboDataConstraints.fill = GridBagConstraints.HORIZONTAL;
        comboDataConstraints.gridx = 4;
        comboDataConstraints.gridy = 0;
        comboDataConstraints.weightx = 1.0 / 5.0;
        comboDataConstraints.insets = new Insets(0, 5, 0, 5);
        configListBox = new ComboBox() {

            private static final long serialVersionUID = 3362542307079178315L;

            @Override
            public void setSelectedItem(Object anObject) {
                super.setSelectedItem(anObject);
                if (anObject != null) {
                    String config = (String) anObject;
                    loadConfigFileViewer.setParameter(config);
                }
            }

        };
        // CONTROLGUI-331: keep long values in tooltip
        DefaultValueListCellRenderer renderer = new DefaultValueListCellRenderer(configListBox) {

            private static final long serialVersionUID = -4914293764219640048L;

            @Override
            protected void applyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
                    javax.swing.JList<? extends Object> list, Object value, int index, boolean isSelected,
                    boolean cellHasFocus, boolean derive) {
                String tooltip = tooltipText;
                Object[] configs = configListBox.getValueList();
                if ((configs != null) && (index > -1) && (index < configs.length) && (configs[index] != null)) {
                    tooltip = configs[index].toString();
                }
                super.applyToComponent(comp, font, foreground, tooltip, list, value, index, isSelected, cellHasFocus,
                        derive);
            }
        };
        renderer.setSameTextAndTooltip(false);
        configListBox.setRenderer(renderer);

        controlsActionPanel.add(configListBox, comboDataConstraints);
        controlsActionPanel.add(loadConfigFileViewer, clearDataConstraints);
        controlsActionPanel.add(startCommandViewer, startConstraints2);
        controlsActionPanel.add(stopCommandViewer, stopConstraints);
        controlsActionPanel.add(recordingConfigButton, configConstraints);

        updateControlComponents();

        int x = 0, y = 0;

        GridBagConstraints controlSettingConstraints = new GridBagConstraints();
        controlSettingConstraints.fill = GridBagConstraints.BOTH;
        controlSettingConstraints.gridx = x;
        controlSettingConstraints.gridy = y++;
        controlSettingConstraints.weightx = 1;
        controlSettingConstraints.weighty = 1;
        controlsPanel.add(controlsSettingsScrollPane, controlSettingConstraints);

        GridBagConstraints controlActionConstraints = new GridBagConstraints();
        controlActionConstraints.fill = GridBagConstraints.HORIZONTAL;
        controlActionConstraints.gridx = x;
        controlActionConstraints.gridy = y++;
        controlActionConstraints.weightx = 1;
        controlActionConstraints.weighty = 0;
        controlActionConstraints.insets = new Insets(2, 0, 0, 0);
        controlsPanel.add(controlsActionPanel, controlActionConstraints);
        controlsPanel.setBorder(new TitledBorder(CONTROLS));
    }

    protected void updateControlComponents() {
        loadConfigFileViewer.setVisible(deviceType.isLoadConfigFileEnabled());
        configListBox.setVisible(deviceType.isLoadConfigFileEnabled());
        int y = controlAttributesLabels.length;
        List<String> controlAttributes = deviceType.getControlAttributes();
        int count = controlAttributes.size();
        if (y != count) {
            JLabel[] newLabels = Arrays.copyOf(controlAttributesLabels, count);
            Label[] newViewers = Arrays.copyOf(controlAttributesViewers, count);
            IComponent[] newSetters = Arrays.copyOf(controlAttributesSetters, count);
            if (count < y) {
                for (int i = count; i < y; i++) {
                    controlsSettingsPanel.remove(controlAttributesLabels[i]);
                    controlsSettingsPanel.remove(controlAttributesViewers[i]);
                    controlsSettingsPanel.remove((JComponent) controlAttributesSetters[i]);
                    cleanWidget(controlAttributesViewers[i]);
                    cleanWidget(controlAttributesSetters[i]);
                }
            } else {
                for (int i = 0; i < y; i++) {
                    newLabels[i].setText(controlAttributes.get(i));
                    newLabels[i].revalidate();
                }
                while (y < count) {
                    String attribute = controlAttributes.get(y);
                    newLabels[y] = generateTitleLabel(deviceType.getAttributeTitle(attribute));
                    newViewers[y] = generateLabel();
                    newViewers[y].setCometeFont(TITLE_COMETE_FONT);
                    IComponent setter;
                    if (deviceType.mayBeWritable(attribute)) {
                        Number[] stepConfiguration = deviceType.getStepConfiguration(attribute);
                        if (stepConfiguration == null) {
                            Object[] possibleValues = deviceType.getPossibleValues(attribute);
                            if (possibleValues == null) {
                                TextField field = generateTextField();
                                field.setColumns(10);
                                field.setCometeFont(newViewers[y].getCometeFont());
                                setter = field;
                            } else {
                                ComboBox combo = new ComboBox();
                                combo.setValueList(possibleValues);
                                setter = combo;
                            }
                        } else {
                            Spinner spinner = generateNumberSpinner(stepConfiguration[0], stepConfiguration[1],
                                    stepConfiguration[2], stepConfiguration[3]);
                            spinner.setCometeFont(newViewers[y].getCometeFont());
                            setter = spinner;
                        }
                        ((JComponent) setter).setVisible(false);
                    } else {
                        setter = null;
                    }
                    newSetters[y] = setter;
                    GridBagConstraints labelConstraints = generateTitleLabelConstraints(0, y);
                    GridBagConstraints viewerConstraints = generateViewerConstraints(1, y);
                    controlsSettingsPanel.add(newLabels[y], labelConstraints);
                    controlsSettingsPanel.add(newViewers[y], viewerConstraints);
                    if (setter instanceof JComponent) {
                        GridBagConstraints setterConstraints = generateSetterConstraints(2, y);
                        controlsSettingsPanel.add((JComponent) setter, setterConstraints);
                    }
                    y++;
                }
            }
            controlAttributesLabels = newLabels;
            controlAttributesViewers = newViewers;
            controlAttributesSetters = newSetters;
            controlsSettingsPanel.revalidate();
            controlsSettingsScrollPane.revalidate();
            controlsPanel.revalidate();
        }
    }

    public void addActionListenerOnStartButton(ActionListener listener) {
        ((JButton) startCommandViewer).addActionListener(listener);
    }

    public void removeActionListenerFromStartButton(ActionListener listener) {
        ((JButton) startCommandViewer).removeActionListener(listener);
    }

    public void addActionListenerOnStopButton(ActionListener listener) {
        ((JButton) stopCommandViewer).addActionListener(listener);
    }

    public void removeActionListenerFromStopButton(ActionListener listener) {
        ((JButton) stopCommandViewer).removeActionListener(listener);
    }

    /**
     * 
     * @return JTextField
     */
    public JComboBox<Integer> getRoiIndex() {
        if (roiIndexSelection == null) {
            roiIndexSelection = new JComboBox<>();
            roiIndexSelection.setToolTipText("ROIs Number");
            for (int i = 0; i < roiSize.length; i++) {
                roiIndexSelection.addItem(i + 1);
            }
        }
        return roiIndexSelection;
    }

    protected void connectRecordConfig() {
        TangoKey key;
        key = generateAttributeKey(FILE_GENERATION);
        setWidgetModel(fileGenerationBox, booleanBox, key);
        key = generateAttributeKey(STREAM_TYPE);
        setWidgetModel(streamTypeBox, stringBox, key);
        key = generateAttributeKey(STREAM_TARGET_PATH);
        setWidgetModel(streamTargetPathField, stringBox, key);
        key = generateAttributeKey(STREAM_TARGET_FILE);
        setWidgetModel(streamTargetFileField, stringBox, key);
        key = generateAttributeKey(STREAM_NB_ACQ_PER_FILE);
        setWidgetModel(streamNbAcqPerFileField, stringBox, key);
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            booleanBox.setUserEnabled(fileGenerationBox, true);
            fileGenerationBox.updateSelection(true);
            recordingConfigButton.setEnabled(true);
        });
    }

    protected void closeRecordConfigDialog() {
        if ((recordConfigDialog != null) && recordConfigDialog.isVisible()) {
            recordConfigDialog.dispose();
        }
    }

    protected void cleanRecordConfig() {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            closeRecordConfigDialog();
            recordingConfigButton.setEnabled(false);
        });
        cleanWidget(fileGenerationBox);
        cleanWidget(streamTypeBox);
        cleanWidget(streamTargetFileField);
        cleanWidget(streamNbAcqPerFileField);
    }

    /**
     * 
     * @return JPanel
     */
    public JPanel getSpeedRadioButtonPanel() {
        if (speedRadioButtonPanel == null) {
            speedRadioButtonPanel = new JPanel();
            speedRadioButtonPanel.setLayout(new BorderLayout());
            speedRadioButtonPanel.setBorder(BorderFactory.createTitledBorder("Amplifier Selection"));
            speedRadioButtonPanel.setLayout(new GridLayout(1, 2));
            speedRadioButtonPanel.add(getComboCommandPanel());
        }
        return speedRadioButtonPanel;
    }

    private JPanel getComboCommandPanel() {
        if (comboCommandPanel == null) {
            comboCommandPanel = new JPanel(new BorderLayout());
            comboCommandLabel = new JLabel("Amplifier Values ");
            comboCommandPanel.add(comboCommandLabel, BorderLayout.WEST);
            comboCommandPanel.add(getComboCommandViewer(), BorderLayout.CENTER);
        }
        return comboCommandPanel;
    }

    /**
     * 
     * @return OptionComboCommandViewer
     */
    public ComboBox getComboCommandViewer() {
        if (comboCommandViewer == null) {
            comboCommandViewer = new ComboBox();
        }
        return comboCommandViewer;
    }

    @Override
    public void updateComponentsState(DeviceMode mode, String state) {
        boolean standby = STANDBY.equalsIgnoreCase(state);
        List<String> controlAttributes = deviceType.getControlAttributes();
        if (controlAttributes != null) {
            int index = 0;
            for (IComponent setter : controlAttributesSetters) {
                if ((setter != null) && (index < controlAttributes.size())) {
                    setter.setVisible(standby && deviceType.isWritableInMode(controlAttributes.get(index), mode));
                }
                index++;
            }
        }
        booleanBox.setUserEnabled(fileGenerationBox, standby);
        recordingConfigButton.setEnabled(standby);
        stringBox.setUserEnabled(loadConfigFileViewer,
                standby || OFF.equalsIgnoreCase(state) || FAULT.equalsIgnoreCase(state));
        boolean enabled = standby && fileGenerationBox.isSelected();
        stringBox.setUserEnabled(streamTargetPathField, enabled);
        stringBox.setUserEnabled(streamTargetFileField, enabled);
        stringBox.setUserEnabled(streamNbAcqPerFileField, enabled);
    }

    @Override
    public void updateSplitPaneDividerLocation() {
        mainSplitPane.setDividerLocation(0.6);
        dataSplitPane.setDividerLocation(0.5);
    }

    protected void cleanRoiModels() {
        if (roiScalarViewersValues != null) {
            for (int i = 0; i < MAX_ROIS; i++) {
                Label viewer = roiScalarViewersValues[i];
                stringBox.setErrorText(viewer, ObjectUtils.EMPTY_STRING);
                cleanWidget(viewer);
                cleanWidget(hroiButtons[i]);
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    viewer.setText(ObjectUtils.EMPTY_STRING);
                });
            }
        }
    }

    protected void connectRoiModels() {
        TangoKey key = null;
        if (roiScalarViewersValues != null) {
            for (int i = 0; i < roiSize.length; i++) {
                key = null;
                if (roiScalarViewersValues[i] != null) {
                    if (currentChannel > -1) {
                        key = generateAttributeKey(ROI + channelFormat.format(currentChannel) + INDEX_SEPARATOR
                                + channelFormat.format(roiSize[i]));
                    }
                    if (key != null) {
                        stringBox.setColorEnabled(roiScalarViewersValues[i], false);
                        stringBox.setErrorText(roiScalarViewersValues[i], null);
                        setWidgetModel(roiScalarViewersValues[i], stringBox, key);
                    }
                }
            }
        }
    }

    protected void reconnectRoiModels() {
        cleanRoiModels();
        connectRoiModels();
    }

    @Override
    public String[] prepareSetRoiButton() {
        String[] toReturn = null;
        if ((startTextFields != null) && (endTextFields != null)) {
            Collection<String> result = new ArrayList<>();
            for (int i = 0; i < startTextFields.length; i++) {
                String start = checkText(startTextFields[i]), end = checkText(endTextFields[i]);
                if ((start != null) && (end != null)) {
                    result.add(start);
                    result.add(end);
                }
            }
            // rois can be set
            // apply starts and ends
            toReturn = result.toArray(new String[result.size()]);
            result.clear();
        }
        return toReturn;
    }

    protected static String checkText(JTextField textField) {
        String txt;
        if (textField == null) {
            txt = null;
        } else {
            txt = textField.getText();
            if (txt != null) {
                txt = txt.trim();
                if (txt.isEmpty()) {
                    txt = null;
                } else {
                    try {
                        // ROI start and end are positive int values
                        Integer.parseInt(txt);
                    } catch (Exception e) {
                        txt = null;
                    }
                }
            }
        }
        return txt;
    }

    protected void enableSetRoi(JTextField source) {
        if (source != null && source instanceof ErrorTextField) {
            ((ErrorTextField) source).setInError(false);
        }
        if (setROIsButtonViewer != null) {
            setROIsButtonViewer.setEnabled(true);
        }
    }

    @Override
    public void refreshRoisTextFields() {
        int roiCount = 0;
        int[] rois;
        for (int i = 0; i < channelCount; i++) {
            rois = roiListTarget.getRois(i);
            if (rois != null) {
                roiCount = Math.max(roiCount, rois.length / 2);
            }
        }
        if (roiCount != 0) {
            requestedROIs = roiCount;
        }
        for (int i = 0; i < MAX_ROIS; i++) {
            startTextFields[i].setText(ObjectUtils.EMPTY_STRING);
            startTextFields[i].setErrorColor(getBackground());
            startTextFields[i].setInError(true);
            endTextFields[i].setText(ObjectUtils.EMPTY_STRING);
            endTextFields[i].setErrorColor(getBackground());
            endTextFields[i].setInError(true);
            if (i < requestedROIs) {
                roiLabels[i].setVisible(true);
                startTextFields[i].setVisible(true);
                endTextFields[i].setVisible(true);
            } else {
                roiLabels[i].setVisible(false);
                startTextFields[i].setVisible(false);
                endTextFields[i].setVisible(false);
            }
        }
        int[] value = lastRoiValue;
        if (value != null) {
            for (int i = 0; i < value.length / 2; i++) {
                if ((startTextFields != null) && (startTextFields.length > i) && (startTextFields[i] != null)) {
                    startTextFields[i].setInError(false);
                    startTextFields[i].setText(String.valueOf(value[2 * i]));
                }
                if ((endTextFields != null) && (endTextFields.length > i) && (endTextFields[i] != null)) {
                    endTextFields[i].setInError(false);
                    endTextFields[i].setText(String.valueOf(value[2 * i + 1]));
                }
            }
        }
    }

    @Override
    public void refreshRoiTable(DeviceMode mode) {
        boolean specialMode = deviceType.isSpecialRoiMode(mode),
                classicMode = deviceType.areRoisEnabledInMode(mode) && !specialMode;
        int max = Math.min(requestedROIs, MAX_ROIS);
        roiValuesLabel.setVisible(classicMode);
        for (int i = 0; i < max; i++) {
            roiScalarViewersValues[i].setVisible(classicMode);
            hroiButtons[i].setVisible(specialMode);
        }
        for (int i = max; i < MAX_ROIS; i++) {
            roiScalarViewersValues[i].setVisible(false);
            hroiButtons[i].setVisible(false);
        }
    }

    /**
     * This method should return an instance of this class which does NOT initialize it's GUI
     * elements. This method is ONLY required by Jigloo if the superclass of this class is abstract
     * or non-public. It is not needed in any other situation.
     */
    public static Object getGUIBuilderInstance() {
        return new MCABean(false);
    }

    public double getCalibrationParameterA() {
        return calibrationParameterA;
    }

    public void setCalibrationParameterA(double calibrationParameterA) {
        this.calibrationParameterA = calibrationParameterA;
    }

    public double getCalibrationParameterB() {
        return calibrationParameterB;
    }

    public void setCalibrationParameterB(double calibrationParameterB) {
        this.calibrationParameterB = calibrationParameterB;
    }

    public long getNumberChannels() {
        return numberChannels;
    }

    public void setNumberChannels(long numberChannels) {
        this.numberChannels = numberChannels;
    }

    public MCADevice getDeviceType() {
        return deviceType;
    }

    public String getDeviceTypeName() {
        MCADevice deviceType = this.deviceType;
        return deviceType == null ? UNKNOWN_DEVICE : deviceType.toString();
    }

    public MultiRoiNonAttrNumberSpectrumViewer getDataViewer() {
        return dataViewer;
    }

    protected String getMCAModel() {
        return getModel();
    }

    /**
     * 
     * @return String
     */
    public String getBeamLineEnergyDeviceName() {
        return beamLineEnergyDeviceName;
    }

    /**
     * 
     * @param beamLineEnergyDeviceName
     */
    public void setBeamLineEnergyDeviceName(String beamLineEnergyDeviceName) {
        if (beamLineEnergyDeviceName == null) {
            beamLineEnergyDeviceName = ObjectUtils.EMPTY_STRING;
        }
        if (!beamLineEnergyDeviceName.isEmpty()) {
            this.beamLineEnergyDeviceName = beamLineEnergyDeviceName;
        }
    }

    @Override
    public boolean isShowAllChannelsInChart() {
        return showAllChannelsInChart || (currentChannel == -1);
    }

    @Override
    public void setShowAllChannelsInChart(boolean showAllChannelsInChart) {
        this.showAllChannelsInChart = showAllChannelsInChart;
        showAllChannelsInChartBox.setSelected(showAllChannelsInChart);
        dataViewer.removeROIs(showAllChannelsInChart);
        setChartModel();
    }

    private Spinner generateNumberSpinner(Number min, Number max, Number step, Number value) {
        Spinner spinner = new Spinner();
        spinner.setCometeFont(CometeFont.DEFAULT_FONT);
        spinner.setNumberValue(value);
        spinner.setMinimum(min);
        spinner.setMaximum(max);
        spinner.setStep(step);
        spinner.setMinimumSize(spinner.getPreferredSize());
        return spinner;
    }

    protected JLabel generateTitleLabel(String text) {
        JLabel inputLabel = text == null ? new JLabel() : new JLabel(text);
        inputLabel.setFont(TITLE_FONT);
        inputLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        inputLabel.setVerticalAlignment(SwingConstants.CENTER);
        return inputLabel;
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // not managed
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // not managed
    }

    @Override
    public String loadROIs(File roiFile) throws RoiFileException {
        String result = null;
        if (roiFile == null) {
            throw new RoiFileException("Can't load ROIs from null file");
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader(roiFile))) {
                StringBuilder builder = new StringBuilder();
                String line;
                boolean allChannelsSet = false;
                while ((line = reader.readLine()) != null) {
                    line = line.trim();
                    if (!line.isEmpty()) {
                        // Check file format
                        String[] split = line.split(ROI_SEPARATOR);
                        if (split.length % 2 == 1) {
                            try {
                                int channel = Integer.MIN_VALUE;
                                int i = 0;
                                int lastValue = -1;
                                for (String strval : split) {
                                    int val = Integer.parseInt(strval.trim());
                                    if (val < 0) {
                                        if ((channel != Integer.MIN_VALUE) || (val != -1)) {
                                            throw new RoiFileException(
                                                    roiFile.getAbsolutePath() + IS_NOT_A_VALID_ROI_FILE);
                                        }
                                    }
                                    if (channel == Integer.MIN_VALUE) {
                                        channel = val;
                                        if (allChannelsSet) {
                                            throw new RoiFileException(roiFile.getAbsolutePath()
                                                    + CAN_T_DEFINE_ROIS_FOR_A_CHANNEL_WHEN_ALREADY_DEFINED_FOR_ALL_CHANNELS);
                                        } else if (channel < 0) {
                                            allChannelsSet = true;
                                            if (builder.length() > 0) {
                                                throw new RoiFileException(roiFile.getAbsolutePath()
                                                        + CAN_T_DEFINE_ROIS_FOR_ALL_CHANNELS_WHEN_ALREADY_DEFINED_FOR_AT_LEAST_1_CHANNEL);

                                            }
                                        }
                                    } else {
                                        if (i % 2 == 1 && val <= lastValue) {
                                            throw new RoiFileException(roiFile.getAbsolutePath()
                                                    + ROI_END_MUST_BE_ROI_START + channel + CLOSE_PARENTHESIS);
                                        }
                                        lastValue = val;
                                        i++;
                                    }
                                }
                                // File format is OK: add line to StringBuilder
                                builder.append(line).append(LIST_SEPARATOR);
                            } catch (NumberFormatException e) {
                                throw new RoiFileException(roiFile.getAbsolutePath() + IS_NOT_A_VALID_ROI_FILE, e);
                            }
                        } else {
                            throw new RoiFileException(roiFile.getAbsolutePath() + IS_NOT_A_VALID_ROI_FILE);
                        }
                    } // end if (!line.isEmpty())
                } // end while ((line = reader.readLine()) != null)
                  // remove last ","
                if (builder.length() > 0) {
                    builder.delete(builder.length() - LIST_SEPARATOR.length(), builder.length());
                }
                result = builder.toString();
                if (builder.length() > 0) {
                    builder.delete(0, builder.length());
                }
            } catch (IOException e) {
                throw new RoiFileException(FAILED_TO_LOAD_ROIS_FROM + roiFile.getAbsolutePath(), e);
            }
        } // end if (roiFile == null) ... else
        return result;
    }

    @Override
    public void writeRoiFile(File roiFile, String roiFileContent) throws RoiFileException {
        if (roiFile == null) {
            throw new RoiFileException(CAN_T_SAVE_ROIS_IN_NULL_FILE);
        } else if (roiFileContent == null) {
            throw new RoiFileException(NOTHING_TO_SAVE);
        } else {
            try (FileWriter writer = new FileWriter(roiFile)) {
                writer.write(roiFileContent);
                writer.flush();
            } catch (IOException e) {
                throw new RoiFileException(FAILED_TO_SAVE_ROIS_IN + roiFile.getAbsolutePath(), e);
            }
        } // end if (roiFile == null) ... else
    }

}