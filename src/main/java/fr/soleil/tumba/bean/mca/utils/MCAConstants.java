package fr.soleil.tumba.bean.mca.utils;

public interface MCAConstants {

    // Device classes
    public static final String XIA_DXP = "XiaDxp";
    public static final String DANTE_DPP = "DanteDpp";
    public static final String XSPRESS3 = "Xspress3";
    public static final String UNKNOWN_DEVICE = "unknown";
    public static final String PUBLISHER_DEVICE = "Publisher";
    public static final String FACADE_DEVICE = "FacadeDevice";

    // Properties
    public static final String DEVICE_TYPE = "DeviceType";

    // Attributes
    public static final String LOG = "log";
    public static final String BOARD_TYPE = "boardType";
    public static final String CURRENT_ALIAS = "currentAlias";
    public static final String CURRENT_CONFIG_FILE = "currentConfigFile";
    public static final String CURRENT_MODE = "currentMode";
    public static final String CURRENT_PIXEL = "currentPixel";
    public static final String CURRENT_FRAME = "currentFrame";
    public static final String DEAD_TIME = "deadTime";
    public static final String EVENTS_IN_RUN = "eventsInRun";
    public static final String FILE_GENERATION = "fileGeneration";
    public static final String INPUT_COUNT_RATE = "inputCountRate";
    public static final String LIVE_TIME = "liveTime";
    public static final String NB_BINS = "nbBins";
    public static final String NB_CHANNELS = "nbChannels";
    public static final String NB_MODULES = "nbModules";
    public static final String NB_PIXELS = "nbPixels";
    public static final String NB_FRAMES = "nbFrames";
    public static final String OUTPUT_COUNT_RATE = "outputCountRate";
    public static final String PRESET_TYPE = "presetType";
    public static final String PRESET_VALUE = "presetValue";
    public static final String REAL_TIME = "realTime";
    public static final String STREAM_NB_ACQ_PER_FILE = "streamNbAcqPerFile";
    public static final String STREAM_TARGET_FILE = "streamTargetFile";
    public static final String STREAM_TARGET_PATH = "streamTargetPath";
    public static final String STREAM_TYPE = "streamType";
    public static final String TRIGGER_LIVE_TIME = "triggerLiveTime";
    public static final String TRIGGER_MODE = "triggerMode";
    public static final String SP_TIME = "spTime";
    public static final String CHANNEL = "channel";
    public static final String EXPOSURE_TIME = "exposureTime";

    // Attribute values
    public static final String CSV_STREAM = "CSV_STREAM";
    public static final String NEXUS_STREAM = "NEXUS_STREAM";
    public static final String LOG_STREAM = "LOG_STREAM";

    // Commands
    public static final String INIT = "Init";
    public static final String SNAP = "Snap";
    public static final String STOP = "Stop";
    public static final String SET_ROIS_FROM_LIST = "SetRoisFromList";
    public static final String GET_ROIS = "GetRois";
    public static final String REMOVE_ROIS = "RemoveRois";
    public static final String LOAD_CONFIG_FILE = "LoadConfigFile";
    public static final String GET_CONFIGURATIONS_FILES_ALIAS = "GetConfigurationsFilesAlias";

    // States
    public static final String STANDBY = "STANDBY";
    public static final String OFF = "OFF";
    public static final String FAULT = "FAULT";

}
