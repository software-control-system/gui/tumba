package fr.soleil.tumba.bean.mca.utils;

import fr.soleil.data.target.scalar.INumberTarget;

public interface IChannelManager {

    public int getCurrentChannel();

    public void channelChanged(double channelIndex);

    public INumberTarget getChannelSetter();

    public boolean isShowAllChannelsInChart();

    public void setShowAllChannelsInChart(boolean showAllChannelsInChart);
}
