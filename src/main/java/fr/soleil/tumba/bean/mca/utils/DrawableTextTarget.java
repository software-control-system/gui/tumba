package fr.soleil.tumba.bean.mca.utils;

import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.IDrawableTarget;
import fr.soleil.data.target.scalar.ITextTarget;

/**
 * An {@link ITextTarget} that is also an {@link IDrawableTarget}.
 * 
 * @author GIRARDOT
 */
public abstract class DrawableTextTarget implements ITextTarget, IDrawableTarget {

    public DrawableTextTarget() {
        super();
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

}
