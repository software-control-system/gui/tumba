package fr.soleil.tumba.bean.mca.utils;

import fr.soleil.lib.project.awt.ColorUtils;

public interface RoiConstants {

    public static final String ROI_SEPARATOR = ";";
    public static final String LIST_SEPARATOR = ",";

    public static final int[] ROI_COLORS = new int[] { ColorUtils.toRGBA(250, 165, 255, 255),
            ColorUtils.toRGBA(190, 220, 255, 255), ColorUtils.toRGBA(170, 255, 170, 255),
            ColorUtils.toRGBA(255, 220, 150, 255), ColorUtils.toRGBA(235, 235, 235, 255),
            ColorUtils.toRGBA(100, 255, 200, 255), ColorUtils.toRGBA(255, 200, 230, 255),
            ColorUtils.toRGBA(150, 220, 255, 255), ColorUtils.toRGBA(220, 255, 100, 255),
            ColorUtils.toRGBA(200, 200, 255, 255), ColorUtils.toRGBA(150, 250, 255, 255),
            ColorUtils.toRGBA(200, 255, 185, 255), ColorUtils.toRGBA(255, 230, 200, 255),
            ColorUtils.toRGBA(210, 210, 255, 255), ColorUtils.toRGBA(230, 255, 190, 255),
            ColorUtils.toRGBA(255, 190, 220, 255), ColorUtils.toRGBA(210, 240, 255, 255),
            ColorUtils.toRGBA(255, 230, 170, 255), ColorUtils.toRGBA(255, 255, 170, 255),
            ColorUtils.toRGBA(255, 220, 230, 255) };

}
