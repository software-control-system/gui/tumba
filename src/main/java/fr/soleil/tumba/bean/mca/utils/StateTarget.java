package fr.soleil.tumba.bean.mca.utils;

import java.lang.ref.WeakReference;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.ObjectUtils;

public class StateTarget extends DrawableTextTarget {

    private volatile String state;
    private final WeakReference<IModeAndStateManager> modeAndStateManagerRef;
    private final WeakReference<CurrentModeTarget> currentModeTargetRef;

    public StateTarget(IModeAndStateManager modeAndStateManager, CurrentModeTarget currentModeTarget) {
        super();
        state = StringScalarBox.DEFAULT_ERROR_STRING;
        modeAndStateManagerRef = modeAndStateManager == null ? null : new WeakReference<>(modeAndStateManager);
        currentModeTargetRef = currentModeTarget == null ? null : new WeakReference<>(currentModeTarget);
    }

    @Override
    public String getText() {
        return state;
    }

    @Override
    public void setText(String text) {
        this.state = text == null ? StringScalarBox.DEFAULT_ERROR_STRING : text;
        IModeAndStateManager modeAndStateManager = ObjectUtils.recoverObject(modeAndStateManagerRef);
        CurrentModeTarget currentModeTarget = ObjectUtils.recoverObject(currentModeTargetRef);
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            if (modeAndStateManager != null) {
                modeAndStateManager.updateComponentsState(
                        currentModeTargetRef == null ? DeviceMode.UNKNOWN : currentModeTarget.getMode(), this.state);
            }
        });
    }

}