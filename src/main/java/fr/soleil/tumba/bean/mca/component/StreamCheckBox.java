package fr.soleil.tumba.bean.mca.component;

import java.awt.event.ItemEvent;
import java.lang.ref.WeakReference;
import java.util.EventObject;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.tumba.bean.mca.utils.MCAConstants;

public class StreamCheckBox extends CheckBox implements MCAConstants {

    private static final long serialVersionUID = 8226328007156690302L;

    private boolean isReadyToSend;

    private Boolean previousValue;
    private final WeakReference<MemoryComboBox> streamTypeBoxRef;
    private final WeakReference<ReadyTextField> streamTargetPathFieldRef;
    private final WeakReference<ReadyTextField> streamTargetFileFieldRef;
    private final WeakReference<ReadyTextField> streamNbAcqPerFileFieldRef;
    private final WeakReference<StringScalarBox> stringBoxRef;

    public StreamCheckBox(MemoryComboBox streamTypeBox, ReadyTextField streamTargetPathField,
            ReadyTextField streamTargetFileField, ReadyTextField streamNbAcqPerFileField, StringScalarBox stringBox) {
        super();
        isReadyToSend = false;
        streamTypeBoxRef = streamTypeBox == null ? null : new WeakReference<>(streamTypeBox);
        streamTargetPathFieldRef = streamTargetPathField == null ? null : new WeakReference<>(streamTargetPathField);
        streamTargetFileFieldRef = streamTargetFileField == null ? null : new WeakReference<>(streamTargetFileField);
        streamNbAcqPerFileFieldRef = streamNbAcqPerFileField == null ? null
                : new WeakReference<>(streamNbAcqPerFileField);
        stringBoxRef = stringBox == null ? null : new WeakReference<>(stringBox);
        updateSelection(false, false);
    }

    protected <C extends ITextTarget> void setEnabled(WeakReference<C> compRef, boolean enabled) {
        ITextTarget comp = ObjectUtils.recoverObject(compRef);
        StringScalarBox stringBox = ObjectUtils.recoverObject(stringBoxRef);
        if ((comp != null) && (stringBox != null)) {
            stringBox.setUserEnabled(comp, enabled);
        }
    }

    protected void setComponentsEnabled(boolean enabled) {
        setEnabled(streamTypeBoxRef, enabled);
        setEnabled(streamTargetPathFieldRef, enabled);
        setEnabled(streamTargetFileFieldRef, enabled);
        setEnabled(streamNbAcqPerFileFieldRef, enabled);
    }

    protected void updateSelection(boolean updateValue, boolean updateEvenIfNotNull) {
        boolean selected = isSelected();
        setComponentsEnabled(selected);
        if (updateValue && (updateEvenIfNotNull || (previousValue == null))) {
            previousValue = Boolean.valueOf(selected);
        }
    }

    public void updateSelection(boolean mayUpdateValue) {
        updateSelection(mayUpdateValue, false);
    }

    public void sendAction() {
        this.isReadyToSend = true;
        fireSelectedChanged(new EventObject(this));
        previousValue = isSelected();
    }

    @Override
    public void fireSelectedChanged(EventObject event) {
        if (isReadyToSend) {
            super.fireSelectedChanged(event);
            isReadyToSend = false;
        }
    }

    @Override
    protected void transmitItemEvent(ItemEvent event) {
        super.transmitItemEvent(event);
        boolean isSelected = event.getStateChange() == ItemEvent.SELECTED;
        MemoryComboBox streamTypeBox = ObjectUtils.recoverObject(streamTypeBoxRef);
        setComponentsEnabled(isSelected);
        if (isSelected && (streamTypeBox != null) && (streamTypeBox.getSelectedValue() == null)) {
            streamTypeBox.setText(NEXUS_STREAM);
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        updateSelection(true, true);
    }

    public void cancel() {
        setSelected(previousValue == null ? false : previousValue.booleanValue());
    }

}