package fr.soleil.tumba.bean.mca;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author ADIOUF
 *
 */
public class CalibrationSettingsPanel extends JPanel implements Observer {

    private static final long serialVersionUID = 5677968344692435781L;

    protected JPanel calibrationSettingsPanel = null;
    protected JPanel calibrationLine0Panel = null;
    protected JPanel calibrationLine1Panel = null;
    protected JPanel abPanel = null;
    protected JPanel calibrationLine2Panel = null;

    private JTextField channel1TextField;
    private JTextField channel2TextField;

    private JTextField energy1TextField;
    private JTextField energy2TextField;

    /**
     * This is the default constructor
     */
    public CalibrationSettingsPanel() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     *
     * @return void
     */
    private void initialize() {
        this.setLayout(new GridLayout(5, 1));
        this.add(getCalibrationLine0Panel());
        this.add(getCalibrationLine1Panel());
        this.add(getCalibrationLine2Panel());
        this.setBorder(BorderFactory.createTitledBorder("Calibration"));
    }

    public JPanel getCalibrationLine0Panel() {
        if (calibrationLine0Panel == null) {
            calibrationLine0Panel = new JPanel();
            calibrationLine0Panel.setLayout(new GridLayout(1, 2));
            calibrationLine0Panel.add(new JLabel("Channel"));
            calibrationLine0Panel.add(new JLabel("Energy"));
        }
        return calibrationLine0Panel;
    }

    public JPanel getCalibrationLine1Panel() {
        if (calibrationLine1Panel == null) {
            calibrationLine1Panel = new JPanel();
            calibrationLine1Panel.setLayout(new GridLayout(1, 2));
            calibrationLine1Panel.add(getLine1Column0ChannelTextField());
            calibrationLine1Panel.add(getLine1Column1EnergyTextField());
        }
        return calibrationLine1Panel;
    }

    public JTextField getLine1Column0ChannelTextField() {
        if (channel1TextField == null) {
            channel1TextField = new JTextField();
        }
        return channel1TextField;
    }

    public JTextField getLine1Column1EnergyTextField() {
        if (energy1TextField == null) {
            energy1TextField = new JTextField();
        }
        return energy1TextField;
    }

    public JPanel getCalibrationLine2Panel() {
        if (calibrationLine2Panel == null) {
            calibrationLine2Panel = new JPanel();
            calibrationLine2Panel = new JPanel();
            calibrationLine2Panel.setLayout(new GridLayout(1, 3));
            calibrationLine2Panel.add(getLine2Column0ChannelTextField());
            calibrationLine2Panel.add(getLine2Column1EnergyTextField());
        }
        return calibrationLine2Panel;
    }

    public JTextField getLine2Column0ChannelTextField() {
        if (channel2TextField == null) {
            channel2TextField = new JTextField();
        }
        return channel2TextField;
    }

    public JTextField getLine2Column1EnergyTextField() {
        if (energy2TextField == null) {
            energy2TextField = new JTextField();
        }
        return energy2TextField;
    }

    /**
     *
     * @param configuration
     */
    @Override
    public void update(Observable o, Object arg) {

    }

    /**
     *
     * @param currentSelection
     * @param bean
     */
    protected void refreshGUI(String[] currentSelection) {
        channel2TextField.setText(currentSelection[0]);
        channel1TextField.setText(currentSelection[1]);
    }

}
