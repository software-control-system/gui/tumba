package fr.soleil.tumba.bean.mca.utils;

import java.lang.ref.WeakReference;

import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.ObjectUtils;

public class CurrentModeTarget extends DrawableTextTarget implements IDeviceModeContainer {

    private volatile DeviceMode mode;
    private boolean setOnce;
    private final WeakReference<IModeAndStateManager> modeAndStateManagerRef;
    private final WeakReference<IMCADisplayManager> displayManagerRef;
    private final Object refreshLock;

    public CurrentModeTarget(IModeAndStateManager modeAndStateManager, IMCADisplayManager displayManager,
            Object refreshLock) {
        super();
        mode = DeviceMode.UNKNOWN;
        setOnce = false;
        modeAndStateManagerRef = modeAndStateManager == null ? null : new WeakReference<>(modeAndStateManager);
        displayManagerRef = displayManager == null ? null : new WeakReference<>(displayManager);
        this.refreshLock = refreshLock;
    }

    public void reset() {
        setOnce = false;
    }

    @Override
    public DeviceMode getMode() {
        return mode;
    }

    @Override
    public String getText() {
        DeviceMode mode = this.mode;
        return mode == null ? null : mode.toString();
    }

    @Override
    public void setText(String text) {
        DeviceMode newMode = DeviceMode.parse(text);
        if ((newMode != mode) || (!setOnce)) {
            this.mode = newMode;
            final boolean updateSplitPane;
            if (setOnce) {
                updateSplitPane = false;
            } else {
                setOnce = true;
                updateSplitPane = true;
            }
            IMCADisplayManager displayManager = ObjectUtils.recoverObject(displayManagerRef);
            IModeAndStateManager modeAndStateManager = ObjectUtils.recoverObject(modeAndStateManagerRef);
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                if (updateSplitPane && (displayManager != null)) {
                    displayManager.updateSplitPaneDividerLocation();
                }
            });
            synchronized (refreshLock) {
                DeviceMode mode = this.mode;
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    if (modeAndStateManager != null) {
                        modeAndStateManager.updateVisibility(mode);
                    }
                });
                if (modeAndStateManager != null) {
                    modeAndStateManager.updateConnections(mode);
                }
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    if (displayManager != null) {
                        displayManager.displayBadAttributesAndCommands();
                    }
                });
            }
        }
    }
}