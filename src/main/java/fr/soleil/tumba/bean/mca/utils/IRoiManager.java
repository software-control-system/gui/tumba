package fr.soleil.tumba.bean.mca.utils;

import java.io.File;

import javax.swing.JFileChooser;

import fr.soleil.tumba.bean.mca.exception.RoiFileException;

public interface IRoiManager extends IDisplayManager {

    public int[] getLastRoiValue();

    public void setLastRoiValue(int... value);

    public void refreshRoisTextFields();

    public void refreshRoiTable(DeviceMode mode);

    public void refreshDynamicRois();

    public JFileChooser getRoiChooser();

    public String loadROIs(File roiFile) throws RoiFileException;

    public String[] prepareSetRoiButton();

    public void writeRoiFile(File roiFile, String roiFileContent) throws RoiFileException;

}
