package fr.soleil.tumba.bean.mca.utils;

/**
 * An interface for something that manages some MCA display.
 * 
 * @author GIRARDOT
 */
public interface IMCADisplayManager {

    /**
     * Updates the split pane divider location.
     */
    public void updateSplitPaneDividerLocation();

    /**
     * Displays bad attributes and commands.
     */
    public void displayBadAttributesAndCommands();
}
