package fr.soleil.tumba.bean.mca.component;

import java.awt.event.ActionEvent;
import java.lang.ref.WeakReference;

import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.tumba.bean.mca.utils.IChannelCountManager;
import fr.soleil.tumba.bean.mca.utils.IChannelManager;
import fr.soleil.tumba.bean.mca.utils.IRoiManager;

public class SetRoisButton extends ASetRoisButton {

    private static final long serialVersionUID = -7806905183521498137L;

    private final WeakReference<StringButton> clearROIsButtonViewerRef;

    private boolean shouldBeEnabled;

    public SetRoisButton(IRoiManager roiManager, IChannelCountManager channelCountManager,
            IChannelManager channelManager, CheckBox allChannelROI, StringButton clearROIsButtonViewer) {
        super(roiManager, channelCountManager, channelManager, allChannelROI);
        this.clearROIsButtonViewerRef = clearROIsButtonViewer == null ? null
                : new WeakReference<>(clearROIsButtonViewer);
    }

    @Override
    protected void afterValidSetRoiPreparation() {
        shouldBeEnabled = false;
    }

    @Override
    protected void actionOnInvalidCurrentChannel() {
        StringButton clearROIsButtonViewer = ObjectUtils.recoverObject(clearROIsButtonViewerRef);
        if (clearROIsButtonViewer != null) {
            clearROIsButtonViewer
                    .actionPerformed(new ActionEvent(clearROIsButtonViewer, ActionEvent.ACTION_PERFORMED, null));
        }
    }

    @Override
    protected boolean isEmptyParamSupportedFor1Channel() {
        return false;
    }

    @Override
    protected boolean isEmptyParamSupportedForAllChannels() {
        return false;
    }

    @Override
    protected String buildParameterForAllChannels(String channelSeparator, int currentChannel,
            String... preparedParam) {
        String parameter = null;
        if (preparedParam.length > 0) {
            StringBuilder param1 = new StringBuilder();
            IChannelCountManager channelCountManager = ObjectUtils.recoverObject(channelCountManagerRef);
            int channelCount = channelCountManager == null ? 0 : channelCountManager.getChannelCount();
            for (int channelNumber = 0; channelNumber < channelCount; channelNumber++) {
                param1.append(Integer.toString(channelNumber));
                for (String p : preparedParam) {
                    param1.append(ROI_SEPARATOR).append(p);
                }
                param1.append(channelSeparator);
            }
            param1.delete(param1.length() - channelSeparator.length(), param1.length());
            parameter = param1.toString();
            param1.delete(0, param1.length());
        }
        return parameter;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        setParameter(null);
        shouldBeEnabled = true;
        String parameter = buildParameter(LIST_SEPARATOR);
        System.err.println("***** parameter: " + parameter);
        if (parameter != null) {
            IRoiManager roiManager = ObjectUtils.recoverObject(roiManagerRef);
            setParameter(parameter);
            super.actionPerformed(event);
            if (roiManager != null) {
                roiManager.refreshDynamicRois();
            }
        }
        if (!shouldBeEnabled) {
            setEnabled(false);
        }
    }

}