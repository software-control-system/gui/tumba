package fr.soleil.tumba.bean.mca.utils;

/**
 * An interface for something that knows a {@link DeviceMode}.
 * 
 * @author GIRARDOT
 *
 */
public interface IDeviceModeContainer {

    /**
     * Returns the known {@link DeviceMode}.
     * 
     * @return The known {@link DeviceMode}.
     */
    public DeviceMode getMode();

}
