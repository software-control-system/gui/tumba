package fr.soleil.tumba.bean.mca.component;

import java.awt.Color;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * A {@link Chart} that knows some Regions Of Interest (ROI), and displays them with
 * previously set colors and names
 * 
 * @author girardot
 */
public class MultiRoiNonAttrNumberSpectrumViewer extends Chart {

    private static final long serialVersionUID = -2571359783880697892L;

    public static final String ROI_PREFIX = "Roi ";

    protected int[] roiColors;
    protected String[] roiNames;
    protected volatile int[] rois;
    protected int[] currentRois;

    public MultiRoiNonAttrNumberSpectrumViewer() {
        super();
        roiColors = null;
        setAutoScale(true, IChartViewer.Y1);
        setManagementPanelVisible(false);
        setAutoHighlightOnLegend(true);
    }

    public String[] getRoiNames() {
        return roiNames;
    }

    public void setRoiName(String name, int index) {
        if ((roiNames != null) && (index > -1) && (index < roiNames.length)) {
            roiNames[index] = name;
            updateROI(ROI_PREFIX + (index + 1), index);
        }
    }

    /**
     * Sets the ROIs display names
     * 
     * @param roiNames The display names to set
     */
    public void setRoiNames(String[] roiNames) {
        String[] previous = this.roiNames;
        if (previous != null) {
            cleanROIs(previous.length);
        }
        this.roiNames = roiNames;
        if (roiNames != null) {
            updateROIs(roiNames.length);
        }
    }

    /**
     * Return the ROI colors (RGB)
     * 
     * @return An <code>int[]</code>
     */
    public int[] getRoiColors() {
        return roiColors;
    }

    /**
     * Sets the ROI colors (RGB)
     * 
     * @param roiColors the roi colors to set
     */
    public void setRoiColors(int[] roiColors) {
        int[] previous = this.roiColors;
        if (previous != null) {
            cleanROIs(previous.length);
        }
        this.roiColors = roiColors;
        if (roiColors != null) {
            updateROIs(roiColors.length);
        }
    }

    /**
     * Cleans all rois plot properties for a given length
     * 
     * @param length The length
     */
    protected void cleanROIs(int length) {
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                cleanDataViewProperties(ROI_PREFIX + (i + 1));
            }
        }
    }

    /**
     * Updates all rois plot properties for a given length
     * 
     * @param length The length
     */
    protected void updateROIs(int length) {
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                updateROI(ROI_PREFIX + (i + 1), i);
            }
        }
    }

    /**
     * Updates the plot properties if a ROI view
     * 
     * @param id The view id
     * @param index The index of the color to use in roi colors
     */
    protected void updateROI(String id, int index) {
        setDataViewFillStyle(id, IChartViewer.FILL_STYLE_SOLID);
        setDataViewFillMethod(id, IChartViewer.METHOD_FILL_FROM_BOTTOM);
        setDataViewLabelVisible(id, false);
        setDataViewClickable(id, false);
        setDataViewMarkerStyle(id, IChartViewer.MARKER_VERT_LINE);
        int[] colors = roiColors;
        if ((colors != null) && (index < colors.length)) {
            CometeColor roiColor = ColorTool.getCometeColor(new Color(colors[index]));
            setDataViewCometeColor(id, roiColor);
            setDataViewFillCometeColor(id, roiColor);
            setDataViewMarkerCometeColor(id, roiColor);
        }
        String[] names = roiNames;
        if ((names != null) && (index < names.length)) {
            String displayName = names[index];
            if (displayName == null) {
                displayName = id;
            }
            setDataViewDisplayName(id, displayName);
        }
    }

    /**
     * Updates a {@link Map} to insert the ROIs
     * 
     * @return a {@link Map} containing data and rois
     */
    protected Map<String, Object> updateROIsFromMap(Map<String, Object> data) {
        Map<String, Object> toReturn = new LinkedHashMap<String, Object>();
        if (data != null) {
            List<String> toRemove = new ArrayList<String>();
            double max = Double.NEGATIVE_INFINITY;
            for (Entry<String, Object> entry : data.entrySet()) {
                String id = entry.getKey();
                if (id != null) {
                    if (id.startsWith(ROI_PREFIX)) {
                        toRemove.add(id);
                    } else {
                        Object array = entry.getValue();
                        int[] shape = ArrayUtils.recoverShape(array);
                        if ((shape != null) && (shape.length > 0)) {
                            boolean flatArray;
                            if (shape.length == 1) {
                                flatArray = true;
                                if (array instanceof Object[]) {
                                    Object[] objArray = (Object[]) array;
                                    for (Object obj : objArray) {
                                        if (obj != null) {
                                            if (obj.getClass().isArray()) {
                                                flatArray = false;
                                            }
                                            break;
                                        }
                                    }
                                }
                            } else {
                                flatArray = false;
                            }
                            if (flatArray) {
                                max = extractMax(max, array, false);
                            } else {
                                max = extractMax(max, Array.get(array, IChartViewer.Y_INDEX), true);
                            }
                        }
                    }
                }
            }
            for (String id : toRemove) {
                data.remove(id);
            }
            if (Double.isInfinite(max)) {
                max = 0;
            }
            int[] roisToUSe = this.rois;
            if ((roisToUSe != null) && (roisToUSe.length % 2 == 0)) {
                int length = roisToUSe.length / 2;
                for (int i = 0; i < length; i++) {
                    String id = ROI_PREFIX + (i + 1);
                    double[][] roiData = new double[2][2];
                    roiData[IChartViewer.X_INDEX] = new double[] { roisToUSe[2 * i], roisToUSe[2 * i + 1] };
                    roiData[IChartViewer.Y_INDEX] = new double[] { max, max };
                    toReturn.put(id, roiData);
                    updateROI(id, i);
                }
            }
//            toReturn.putAll(data);
        }
        return toReturn;
    }

    @Override
    protected void addData(Map<String, Object> dataToAdd, boolean forceGlobalRepaint, boolean updateTable) {
        super.addData(dataToAdd, false, false);
        super.addData(updateROIsFromMap(getData()), forceGlobalRepaint, updateTable);
    }

    /**
     * Returns this {@link MultiRoiNonAttrNumberSpectrumViewer} rois
     * 
     * @return A <code>double[]</code>
     */
    public int[] getRois() {
        return rois;
    }

    /**
     * Sets this {@link MultiRoiNonAttrNumberSpectrumViewer} rois
     * 
     * @param rois The rois to set
     */
    public void setRois(int[] rois) {
        this.rois = rois;
        super.addData(updateROIsFromMap(getData()), true, true);
    }

    public int[] getCurrentRois() {
        return currentRois;
    }

    public void setCurrentRois(int[] currentRois) {
        this.currentRois = currentRois;
    }

    public void removeROIs(boolean showAllChannelsInChart) {
        if (showAllChannelsInChart) {
            currentRois = rois;
            setRois(null);
        } else {
            setRois(currentRois);
        }
    }

    /**
     * Extracts the maximum in a number array
     * 
     * @param currentMax The current known maximum
     * @param singleDimensionArray The number array
     * @param yOnly Whether the number array represent y values only
     * @return A <code>double</code>
     */
    private double extractMax(double currentMax, Object singleDimensionArray, boolean yOnly) {
        double max = currentMax;
        if (singleDimensionArray != null) {
            int length = Array.getLength(singleDimensionArray);
            if (singleDimensionArray instanceof Object[]) {
                Object[] array = (Object[]) singleDimensionArray;
                if (yOnly) {
                    for (Object data : array) {
                        if (data instanceof Number) {
                            double value = ((Number) data).doubleValue();
                            if ((value > max) && (!Double.isInfinite(value))) {
                                max = value;
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < length / 2; i++) {
                        int index = 2 * i + 1;
                        if (array[index] instanceof Number) {
                            double value = ((Number) array[index]).doubleValue();
                            if ((value > max) && (!Double.isInfinite(value))) {
                                max = value;
                            }
                        }
                    }
                }
            } else {
                if (yOnly) {
                    for (int i = 0; i < length; i++) {
                        double value = Array.getDouble(singleDimensionArray, i);
                        if ((value > max) && (!Double.isInfinite(value))) {
                            max = value;
                        }
                    }
                } else {
                    for (int i = 0; i < length / 2; i++) {
                        int index = 2 * i + 1;
                        double value = Array.getDouble(singleDimensionArray, index);
                        if ((value > max) && (!Double.isInfinite(value))) {
                            max = value;
                        }
                    }
                }
            }
        }
        return max;
    }

}
