package fr.soleil.tumba.bean.mca.utils;

public enum DeviceMode {

    MCA, MAPPING, SCA, UNKNOWN;

    private static final String MODE_MCA = "MCA";
    private static final String MODE_MAPPING = "MAPPING", MODE_MAPPING_FULL = "MAPPING_FULL"; // MAPPING = MAPPING_FULL
    private static final String MODE_SCA = "MAPPING_SCA";
    private static final String MODE_UNKNOWN = "UNKNOWN";

    static {
        // Hack because there is no way to initialize with constants, as constants must be declared after enum values,
        // and enum constructor parameters must exist before enum creation.
        MCA.mode = MODE_MCA;
        MAPPING.mode = MODE_MAPPING_FULL;
        SCA.mode = MODE_SCA;
        UNKNOWN.mode = MODE_UNKNOWN;
    }

    private String mode;

    @Override
    public String toString() {
        return mode;
    }

    /**
     * Parses a {@link DeviceMode} from a {@link String} value.
     * 
     * @param value The {@link String} value.
     * @return A {@link DeviceMode}, never <code>null</code>.
     */
    public static DeviceMode parse(String value) {
        DeviceMode mode = UNKNOWN;
        if (value != null) {
            String modeStr = value.toUpperCase();
            switch (modeStr) {
                case MODE_MCA:
                    mode = MCA;
                    break;
                case MODE_MAPPING:
                case MODE_MAPPING_FULL:
                    mode = MAPPING;
                    break;
                case MODE_SCA:
                    mode = SCA;
                    break;
            }
        }
        return mode;
    }
}
