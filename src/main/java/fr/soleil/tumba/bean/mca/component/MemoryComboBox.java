package fr.soleil.tumba.bean.mca.component;

import java.util.EventObject;

import fr.soleil.comete.swing.ComboBox;

public class MemoryComboBox extends ComboBox {

    private static final long serialVersionUID = 7722876205332836107L;

    private boolean isReadyToSend;

    private String previousValue;

    public MemoryComboBox() {
        super();
        isReadyToSend = false;
    }

    @Override
    public void setText(String text) {
        super.setText(text);
        if (previousValue == null) {
            previousValue = text;
        }
    }

    public void sendAction() {
        this.isReadyToSend = true;
        fireSelectedItemChanged(new EventObject(this));
        previousValue = getText();
    }

    @Override
    public void fireSelectedItemChanged(EventObject event) {
        if (isReadyToSend) {
            super.fireSelectedItemChanged(event);
            isReadyToSend = false;
        }
    }

    public void cancel() {
        setText(previousValue);
    }

}
