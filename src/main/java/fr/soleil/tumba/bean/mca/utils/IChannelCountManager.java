package fr.soleil.tumba.bean.mca.utils;

public interface IChannelCountManager {

    public int getChannelCount();

    public void setChannelCount(int count);
}
