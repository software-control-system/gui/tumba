package fr.soleil.tumba.bean.mca.utils;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.matrix.ITextMatrixTarget;
import fr.soleil.lib.project.ObjectUtils;

public class RoiListTarget implements ITextMatrixTarget, RoiConstants {

    private final Map<Integer, int[]> roiMap;
    private final WeakReference<ChannelListener> channelListenerRef;

    public RoiListTarget(ChannelListener channelListener) {
        roiMap = new ConcurrentHashMap<>();
        channelListenerRef = channelListener == null ? null : new WeakReference<>(channelListener);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        // not managed
        return false;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        // not managed
        return 0;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        // not managed
        return 0;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public String[][] getStringMatrix() {
        // not managed
        return null;
    }

    @Override
    public void setStringMatrix(String[][] value) {
        if ((value == null) || (value.length == 0)) {
            setFlatStringMatrix(null, 0, 0);
        } else if (value.length == 1) {
            String[] array = value[0];
            setFlatStringMatrix(array, array == null ? 0 : array.length, 1);
        } else {
            for (String[] toParse : value) {
                prepareRois(toParse);
            }
            applyChange();
        }
    }

    @Override
    public String[] getFlatStringMatrix() {
        // not managed
        return null;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        if ((value == null) || (value.length == 0)
                || ((value.length == 1) && StringScalarBox.DEFAULT_ERROR_STRING.equals(value[0]))) {
            roiMap.clear();
        } else {
            for (int i = 0; i < value.length; i++) {
                String toParse = value[i];
                if (toParse != null) {
                    prepareRois(toParse.split(ROI_SEPARATOR));
                }
            }
        }
        applyChange();
    }

    protected void applyChange() {
        ChannelListener channelListener = ObjectUtils.recoverObject(channelListenerRef);
        if (channelListener != null) {
            channelListener.updateChannel();
        }
    }

    protected void prepareRois(String[] roisToString) {
        if ((roisToString != null) && (roisToString.length == 1)) {
            String tmp = roisToString[0];
            if ((tmp != null) && tmp.contains(ROI_SEPARATOR)) {
                roisToString = tmp.split(ROI_SEPARATOR);
            }
        }
        buildRois(roisToString);
    }

    protected void buildRois(String[] roisToString) {
        if ((roisToString != null) && (roisToString.length > 0)) {
            int[] rois;
            Integer index = null;
            try {
                index = Integer.valueOf(roisToString[0]);
                rois = new int[roisToString.length - 1];
                for (int i = 0; i < rois.length; i++) {
                    try {
                        rois[i] = Integer.parseInt(roisToString[i + 1]);
                    } catch (Exception e) {
                        rois[i] = (int) Double.parseDouble(roisToString[i + 1]);
                    }
                }
            } catch (Exception e) {
                rois = null;
            }
            if (index != null) {
                if (rois.length > 0) {
                    roiMap.put(index, rois);
                } else {
                    roiMap.remove(index);
                }
            }
        }
    }

    public int[] getRois(int channel) {
        return roiMap.get(Integer.valueOf(channel));
    }

}