package fr.soleil.tumba.bean.mca.utils;

import java.lang.ref.WeakReference;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;
import fr.soleil.tumba.bean.mca.component.MultiRoiNonAttrNumberSpectrumViewer;

public class RoiTarget implements INumberMatrixTarget {

    private final WeakReference<IRoiManager> roiManagerRef;
    private final WeakReference<IChannelManager> channelManagerRef;
    private final WeakReference<CurrentModeTarget> currentModeTargetRef;
    private final WeakReference<MultiRoiNonAttrNumberSpectrumViewer> dataViewerRef;
    private final Object roiLock;

    public RoiTarget(IRoiManager roiManager, IChannelManager channelManager, CurrentModeTarget currentModeTarget,
            MultiRoiNonAttrNumberSpectrumViewer dataViewer, Object roiLock) {
        super();
        this.roiManagerRef = roiManager == null ? null : new WeakReference<IRoiManager>(roiManager);
        this.channelManagerRef = channelManager == null ? null : new WeakReference<>(channelManager);
        this.currentModeTargetRef = currentModeTarget == null ? null : new WeakReference<>(currentModeTarget);
        this.dataViewerRef = dataViewer == null ? null : new WeakReference<>(dataViewer);
        this.roiLock = roiLock;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        // not managed
        return 0;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        // not managed
        return 0;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public Object[] getNumberMatrix() {
        // not managed
        return null;
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        treatRois(NumberArrayUtils.extractIntArray(ArrayUtils.convertArrayDimensionFromNTo1(value)));
    }

    @Override
    public Object getFlatNumberMatrix() {
        // not managed
        return null;
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        treatRois(NumberArrayUtils.extractIntArray(value));
    }

    protected void treatRois(int[] rois) {
        int[] roiValue = (rois == null ? null : rois.clone());
        boolean canSet = false;
        IRoiManager roiManager = ObjectUtils.recoverObject(roiManagerRef);
        IChannelManager channelManager = ObjectUtils.recoverObject(channelManagerRef);
        CurrentModeTarget currentModeTarget = ObjectUtils.recoverObject(currentModeTargetRef);
        MultiRoiNonAttrNumberSpectrumViewer dataViewer = ObjectUtils.recoverObject(dataViewerRef);
        if (roiManager != null) {
            synchronized (roiLock) {
                if (!ObjectUtils.sameObject(roiValue, roiManager.getLastRoiValue())) {
                    roiManager.setLastRoiValue(roiValue);
                    roiManager.refreshRoisTextFields();
                    roiManager.refreshRoiTable(
                            currentModeTarget == null ? DeviceMode.UNKNOWN : currentModeTarget.getMode());
                    canSet = true;
                }
            }
            if (canSet && (dataViewer != null) && (channelManager != null)) {
                dataViewer.setRois(roiValue);
                dataViewer.setCurrentRois(dataViewer.getRois());
                dataViewer.removeROIs(channelManager.isShowAllChannelsInChart());
            }
        }
    }

}
